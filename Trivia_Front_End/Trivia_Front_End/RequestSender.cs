﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    class RequestSender
    {
        public static string Send(byte[] msg, Socket sender)
        {
            byte[] buffer = new byte[1024];

            // Send the data through the socket.  
            int bytesSent = sender.Send(msg);

            // Receive the response from the remote device.  
            int bytesRec = sender.Receive(buffer);

            string answer = Encoding.ASCII.GetString(buffer, 0, bytesRec);
            Console.WriteLine("Echoed test = {0}", answer);

            if (buffer[0] == (byte)Responses.ID_ERR)
            { // Error.
                throw new Exception("Error - bad request usage...");
            }
            return answer.Substring(1);
        }
    }
}
