﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
//using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using Newtonsoft.Json;


namespace Trivia_Front_End
{

    public struct userGameDate
    {
        public int RightAnswers;
        public int WrongAnswers;
        public double averageTimeToAnswer;
    }
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        readonly int sleepTime = 2000; // 2 seconds.

        private Socket sender;
        private bool _isAdmin;
        private RoomData _room;

        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        private int _ticks;

        private int _questionTime;
        private int _numOfQuestions;
        private int _questionCount;
        private DateTime _startTime;
        private TimeSpan _timeSelected;

        private uint _selectedId;
        private float _timeOfAnswer;


        public Game(RoomData Room, Socket socket, bool IsAdmin)
        {
            InitializeComponent();
            this.sender = socket;
            this._isAdmin = IsAdmin;
            this._room = Room;

            // Initialize private fields.
            this._numOfQuestions = Room.NumberOfQuestions;
            this._questionCount = 0;
            this._ticks = 0;
            this._timeOfAnswer = this._questionTime;

            this._questionTime = Room.TimePerQuestion;
            Seconds.Content = this._questionTime;

            // Timer settings.
            DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer(
                System.Windows.Threading.DispatcherPriority.Normal);
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);

            GetQuestion(); // Setting the first question.
            this._startTime = DateTime.Now;
            dispatcherTimer.Start();
        }
        
        private void SetQuestion(string question, List<string> answers)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                Question.Text = question;

                FirstChoice.Content = answers[0];
                SecondChoice.Content = answers[1];
                ThirdChoice.Content = answers[2];
                FourthChoice.Content = answers[3];
            });
        }

        private void SelectAnswer_Callback(object sender, SelectionChangedEventArgs e)
        {
            this._timeSelected = (DateTime.Now - this._startTime); // Calc time and then turn to seconds.
            this._timeOfAnswer = (float)this._timeSelected.Seconds + ((float)this._timeSelected.Milliseconds / 1000);
            this._selectedId = (uint)Answers.SelectedIndex;
        }

        private void ResetListBoxColor()
        {
            Answers.UnselectAll();
            for (int i = 0; i < 4; i++)
            {
                var item = Answers.Items[i] as ListBoxItem;
                item.Background = new SolidColorBrush( // Changing color to back to gray.
                    (Color)ColorConverter.ConvertFromString("#2e3137"));
            }
        }

        private void CheckAnswer(int correctId)
        {
            if (this._selectedId != correctId && this._selectedId < 4) // ListBox Range.
            {
                var selected = Answers.Items[(int)this._selectedId] as ListBoxItem;
                selected.Background = new SolidColorBrush(Colors.Red); // Wrong answer, Changing to the selected to red.
            }
            var correct = Answers.Items[correctId] as ListBoxItem;
            correct.Background = new SolidColorBrush(Colors.Green); // Changing correct answer to green.
        }

        private void DispatcherTimer_Tick(object Sender, EventArgs e)
        {
            this._ticks++;
            int timeLeft = this._questionTime - this._ticks;

            // Setting the caption to the current time.
            Seconds.Content = timeLeft > 0 ? timeLeft.ToString() : "...";

            if (this._ticks == this._questionTime)
            {
                SubmitAnswer();
                Thread thrd = new Thread(() => GetQuestion());
                thrd.Start();
            }
        }

        private void GetQuestion()
        {
            Thread.Sleep(sleepTime); // 2 second break to see result.

            if (_questionCount < _numOfQuestions)
            {
                this.Answers.Dispatcher.Invoke((Action)delegate
                { // Changes in Textbox.
                    ResetListBoxColor(); // Clearing boxes.

                    byte[] msg = new byte[2];
                    msg[0] = (byte)Responses.ID_GET_QUESTION;
                    string recv = RequestSender.Send(msg, this.sender); // Getting next question.

                    GetQuestionResponse ans = JsonConvert.DeserializeObject<GetQuestionResponse>(recv);
                    if (ans.Status)
                    { // Setting next question.
                        SetQuestion(ans.Question, ans.Answers);
                        this._questionCount++;
                    }
                    Seconds.Content = this._questionTime; // Resetting the seconds.
                });

                this._ticks = 0; // Set timer back to 0.
                this._timeOfAnswer = this._questionTime;
                this._startTime = DateTime.Now;
            }
            else
            { // All questions were presented, game ended.
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    EndingScreen newEndingScreen = new EndingScreen(this.sender, this._isAdmin, this._room);
                    newEndingScreen.Show(); // Ending screen.
                    this.Close();
                });
            }
        }

        private void SubmitAnswer()
        {
            SubmitAnswerRequest request = new SubmitAnswerRequest
            {
                Answer_Id = _selectedId,
                Time = _timeOfAnswer
            };
            
            byte[] msg = Serializer.CreateRoomSerializer(request);
            string recv = RequestSender.Send(msg, this.sender);

            SubmitAnswerResponse ans = JsonConvert.DeserializeObject<SubmitAnswerResponse>(recv);
            if (ans.Status)
            { // Check answer.
                CheckAnswer((int)ans.CorrectId);
            }
        }
    }
}
     