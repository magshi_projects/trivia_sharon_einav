﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_Front_End
{

    enum Responses
    {
        ID_ERR, ID_LOGIN, ID_SIGNUP, // Login/Signup.
        ID_LOGOUT, ID_GET_ROOMS, ID_PLAYERS_IN_ROOM, // Menu.
        ID_JOIN_ROOM, ID_CREATE_ROOM, ID_HIGHSCORES, ID_MY_STATUS,
        ID_CLOSE_ROOM, ID_START_GAME, ID_GET_ROOM_STATE, // Room.
        ID_LEAVE_ROOM,
        ID_GET_QUESTION, ID_SUBMIT_ANSWER, // Game
        ID_GAME_RESULTS, ID_LEAVE_GAME
    };

    class MyException : Exception
    {
        private string msg;

        public MyException(string msg)
        {
            this.msg = msg;
        }
        public override string Message => this.msg;
    };


    static class Constants
    {
        public const string RESULT = "Result";
        public const string MESSAGE = "Message";
        public const string STATUS = "Status";
        public const string ROOMS = "Rooms";
        public const string HIGHSCORE = "Highscore";
        public const string QUESTION_COUNT = "QuestionCount";
        public const string TIMEOUT = "Timeout";
        public const string HAS_BEGUN = "HasBegun";
        public const string PLAYERS = "Players";
        public const string CORRECT_ID = "CorrectId";
        public const string QUESTION = "Question";
        public const string ANSWERS = "Answers";
        public const string RESULTS = "Results";
    }
}
