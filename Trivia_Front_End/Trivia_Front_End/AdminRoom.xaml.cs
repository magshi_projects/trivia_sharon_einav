﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using Newtonsoft.Json;


namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for AdminRoom.xaml
    /// </summary>
    public partial class AdminRoom : Window
    {
        readonly int sleepTime = 3000; // 3 seconds.

        private Socket sender;
        private bool InRoom;
        private Thread thread;
        private RoomData adminRoom;


        public AdminRoom(Socket socket, RoomData Room)
        {
            InitializeComponent();
            this.sender = socket;


            this.RoomName.Text = Room.RoomName;
            this.QuestionNum.Text = Room.NumberOfQuestions.ToString();
            this.QuestionTime.Text = Room.TimePerQuestion.ToString();
            this.adminRoom = Room;


            this.InRoom = true; // Setting flag.
            Thread thrd = new Thread(() => Restater());
            thrd.Start();
            this.thread = thrd;
        }

        ~AdminRoom()
        {
            this.InRoom = false; // Room Closing, Extra safety.
            try
            { // Maybe already ended.
                this.Dispatcher.Invoke(() =>
                {
                    this.thread.Abort();
                });
            }
            catch { } // Nothing to do.
        }

        private void Restater()
        {
            while (this.InRoom)
            { // No need to worry about if game has begun, it is the room Admin.
                this.Restate();
                Thread.Sleep(sleepTime); // 3 seconds.
            }
        }

        private void Restate()
        {
            // For safety.
            if (!this.InRoom)
            { // Room Closed.
                this.Dispatcher.Invoke(() =>
                {
                    this.thread.Abort();
                });
            }

            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_GET_ROOM_STATE;
            string recv = RequestSender.Send(msg, this.sender);

            GetRoomStateResponse ans = JsonConvert.DeserializeObject<GetRoomStateResponse>(recv);

            string players = string.Empty;
            for (int i = 0; i < ans.Players.Count; i++)
            {
                players += ans.Players[i];
                if (i + 1 != ans.Players.Count)
                {
                    players += ", ";
                }
            }

            this.Dispatcher.Invoke(() =>
            {
                this.LoggedUsers.Text = players; // Set players in room list.
            });
        }

        private void ExitInThread()
        {
            this.Dispatcher.Invoke(() =>
            {
                this.InRoom = false; // No more in room.
            });

            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                Menu menuWindow = new Menu(this.sender);
                menuWindow.Show();
                this.Close();
            });
        }

        private void btnStartGame_Click(object sender, RoutedEventArgs e)
        {
            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_START_GAME;
            string recv = RequestSender.Send(msg, this.sender);

            StartGameResponse ans = JsonConvert.DeserializeObject<StartGameResponse>(recv);

            if (ans.Status)
            {
                this.InRoom = false;

                Game gameWindow = new Game(adminRoom, this.sender, true);
                gameWindow.Show();
                this.Close();
            }
        }

        private void btnCloseRoom_Click(object sender, RoutedEventArgs e)
        {
            Thread thread = new Thread(() => CloseRoomRequest());
            thread.Start();
        }

        private void CloseRoomRequest()
        {
            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_CLOSE_ROOM;
            string recv = RequestSender.Send(msg, this.sender);

            CloseRoomResponse ans = JsonConvert.DeserializeObject<CloseRoomResponse>(recv);

            if (ans.Status)
            {
                this.ExitInThread();
            }
        }
    }
}
