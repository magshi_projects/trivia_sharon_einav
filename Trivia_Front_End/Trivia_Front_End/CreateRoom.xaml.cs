﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;
using System.Threading;
using System.Text.RegularExpressions;

namespace Trivia_Front_End
{
    public struct RoomData
    {
        public string RoomName;
        public int NumberOfPlayers;
        public int NumberOfQuestions;
        public int TimePerQuestion;

        public RoomData(CreateRoomRequest room)
        {
            RoomName = room.Room_Name;
            NumberOfPlayers = int.Parse(room.Room_Max);
            NumberOfQuestions = room.Room_Count;
            TimePerQuestion = room.Room_Timeout;
        }

        public RoomData(RoomDataResponse room)
        {
            RoomName = room.Name;
            NumberOfPlayers = int.Parse(room.MaxPlayers);
            NumberOfQuestions = (int)room.QuestionCount;
            TimePerQuestion = (int)room.TimePerQuestion;
        }
    }
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Window
    {
        private Socket sender;

        public CreateRoom(Socket socket)
        {
            InitializeComponent();

            this.sender = socket;
        }

        private void btnSignout_Click(object sender, RoutedEventArgs e)
        {
            Menu.Logout(this.sender);
            MainWindow startWindow = new MainWindow();
            startWindow.Show();
            this.Close();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Menu.Logout(this.sender);
            Application.Current.Shutdown();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Menu menuWindow = new Menu(this.sender);
            menuWindow.Show();
            this.Close(); 
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (this.txtNumberOfQuestions.Text.Length == 0 || this.txtNumOfPlayers.Text.Length == 0 || this.txtRoomName.Text.Length == 0 || this.txtTimePerQuestion.Text.Length == 0)
            {
                MessageBox.Show("Please fill all fields.", "Can't Create Room");
            }
            else
            {
                CreateRoomRequest request = new CreateRoomRequest
                {
                    Room_Name = this.txtRoomName.Text,
                    Room_Max = this.txtNumOfPlayers.Text,
                    Room_Count = int.Parse(this.txtNumberOfQuestions.Text),
                    Room_Timeout = int.Parse(this.txtTimePerQuestion.Text)
                };

                Thread thread = new Thread(() => CreateRequest(request));
                thread.Start();
            }
        }

        private void CreateRequest(CreateRoomRequest request)
        {
            byte[] msg = Serializer.CreateRoomSerializer(request);
            string recv = RequestSender.Send(msg, this.sender);

            CreateRoomResponse ans = JsonConvert.DeserializeObject<CreateRoomResponse>(recv);
            if (ans.Status)
            { // room created
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    AdminRoom adminRoomWindow = new AdminRoom(this.sender, new RoomData(request));
                    adminRoomWindow.Show();
                    this.Close();
                });
            }
            else
            {
                MessageBox.Show("Couldn't create room, please try later...", "Creation Error");
            }
        }

        //accepts only numbers
        private void txtNumberOfQuestions_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9]+").IsMatch(e.Text);
        }
        //accepts only numbers
        private void txtNumOfPlayers_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9]+").IsMatch(e.Text);
        }
        //accepts only numbers
        private void txtTimePerQuestion_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9]+").IsMatch(e.Text);
        }
    }
}
