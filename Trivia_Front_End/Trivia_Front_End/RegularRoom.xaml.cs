﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for RegularRoom.xaml
    /// </summary>
    public partial class RegularRoom : Window
    {
        readonly int sleepTime = 3000; // 3 seconds.

        private Socket sender;
        private bool InRoom;
        private Thread thread;
        private RoomData room;

        public RegularRoom(Socket socket, RoomData Room)
        {
            InitializeComponent();
            this.sender = socket;

            this.room = Room;
            this.RoomName.Text = Room.RoomName;
            this.QuestionNum.Text = Room.NumberOfQuestions.ToString();
            this.QuestionTime.Text = Room.TimePerQuestion.ToString();

            this.InRoom = true; // Setting flag.
            Thread thrd = new Thread(() => Restater());
            thrd.Start();
            this.thread = thrd;
        }

        ~RegularRoom()
        {
            this.InRoom = false; // Room Closing, Extra safety.
            try
            { // Maybe already ended.
                this.Dispatcher.Invoke(() =>
                {
                    this.thread.Abort();
                });
            }
            catch { } // Nothing to do.
        }

        private void Restater()
        {
            while(this.InRoom)
            {
                bool hasBegun = this.Restate();

                if(hasBegun)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.InRoom = false; // No more in room.
                    });

                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        Game gameWindow = new Game(this.room, this.sender, false);
                        gameWindow.Show();
                        this.Close();
                    });
                }
                Thread.Sleep(sleepTime); // 3 seconds.
            }
        }

        private bool Restate()
        {
            // For safety.
            if (!this.InRoom)
            { // Room Closed.
                this.Dispatcher.Invoke(() =>
                {
                    this.thread.Abort();
                });
            }

            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_GET_ROOM_STATE;
            string recv = RequestSender.Send(msg, this.sender);

            GetRoomStateResponse ans = JsonConvert.DeserializeObject<GetRoomStateResponse>(recv);
            
            string players = string.Empty;

            if(!ans.Status) // Room Closed, move to menu.
            {
                this.ExitInThread();

                this.Dispatcher.Invoke(() =>
                {
                    this.thread.Abort();
                });
            }

            if(ans.Players != null)
            {
                for (int i = 0; i < ans.Players.Count; i++)
                {
                    players += ans.Players[i];
                    if (i + 1 != ans.Players.Count)
                    {
                        players += ", ";
                    }
                }

                this.Dispatcher.Invoke(() =>
                {
                    this.LoggedUsers.Text = players; // Set players in room list.
                });
            }
            return ans.HasBegun; // Returning flag of if game has begun.
        }

        private void ExitInThread()
        {
            this.Dispatcher.Invoke(() =>
            {
                this.InRoom = false; // No more in room.
            });

            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                Menu menuWindow = new Menu(this.sender);
                menuWindow.Show();
                this.Close();
            });
        }

        private void btnExitRoom_Click(object sender, RoutedEventArgs e)
        {
            Thread thread = new Thread(() => LeaveRoom());
            thread.Start();
        }

        private void LeaveRoom()
        {
            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_LEAVE_ROOM;
            string recv = RequestSender.Send(msg, this.sender);

            LeaveRoomResponse ans = JsonConvert.DeserializeObject<LeaveRoomResponse>(recv);

            if(ans.Status)
            {
                ExitInThread();
            }
        }
    }
}
