﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;

namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Socket sender;
        private IPAddress address;
        private IPEndPoint remoteEP;

        public MainWindow()
        {
            InitializeComponent();

            this.address = IPAddress.Parse("127.0.0.1");
            this.remoteEP = new IPEndPoint(this.address, 8888);
            this.sender = new Socket(this.address.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            try
            {
                sender.Connect(this.remoteEP); // Connecting to server.
                Console.WriteLine("Socket connected to {0}",
                    sender.RemoteEndPoint.ToString());
            }
            catch
            {
                MessageBox.Show("No connection to server", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
        }
        
        public MainWindow(Socket socket)
        {
            InitializeComponent();
            this.sender = socket;
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login loginWindow = new Login(this.sender);
            loginWindow.Show();
            this.Close();
        }

        private void BtnSignup_Click(object sender, RoutedEventArgs e)
        {

            Signup signupWindow = new Signup(this.sender);
            signupWindow.Show();
            this.Close();
        }
    }
}
