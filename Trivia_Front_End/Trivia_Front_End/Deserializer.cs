﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_Front_End
{
    // Responses.
    public struct LoginResponse
    {
        public int Result;
    }

    public struct SignupResponse
    {
        public bool Result;
    }

    public struct LogoutResponse
    {
        public bool Status;
    }

    public struct CloseRoomResponse
    {
        public bool Status;
    }

    public struct StartGameResponse
    {
        public bool Status;
    }

    public struct LeaveRoomResponse
    {
        public bool Status;
    }

    public struct JoinRoomResponse
    {
        public bool Status;
    }

    public struct CreateRoomResponse
    {
        public bool Status;
    }

    public struct GetRoomStateResponse
    {
        public bool Status;
        public uint QuestionCount;
        public uint Timeout;
        public bool HasBegun;
        public List<string> Players;
    }

    public struct GetPlayersInRoomResponse
    {
        public List<LoggedUser> RoomPlayers;
    }

    public struct HighscoreResponse
    {
        public bool Status;
        public Dictionary<string, int> Highscore;
    }

    public struct GetRoomsResponse
    {
        public bool Status;
        public List<RoomDataResponse> Rooms;
    }

    public struct RoomDataResponse
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string MaxPlayers { get; set; }
        public uint QuestionCount { get; set; }
        public uint TimePerQuestion { get; set; }
        public uint IsActive { get; set; }
    }

    public struct LoggedUser
    {
        public string Username;
    }

    public struct GetQuestionResponse
    {
        public bool Status;
        public string Question;
        public List<string> Answers;
    };

    public struct SubmitAnswerResponse
    {
        public bool Status;
        public uint CorrectId;
    };

    public struct LeaveGameResponse
    {
        public bool Status;
    };

    public struct PlayerResults
    {
        public string Username;
        public uint Correct;
        public uint Wrong;
        public float Avg;
    };

    public struct GetGameResultsResponse
    {
        public bool Status;
        public List<PlayerResults> Results;
    };

    public struct MyStatusResponse
    {
        public string Games;
        public string Right;
        public string Wrong;
        public string Avg;
    };
}