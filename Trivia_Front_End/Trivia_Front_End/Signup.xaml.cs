﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    public partial class Signup : Window
    {
        private Socket sender;

        public Signup(Socket socket)
        {
            InitializeComponent();

            this.sender = socket;
        }

        private void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string username = this.txtUsername.Text;
            string password = this.txtPassword.Password;
            string email = this.txtEmail.Text;
            
            if(email.Contains('@'))
            {
                Thread thread = new Thread(() => SignupRequest(username, password, email));
                thread.Start();
            }
            else
            {
                MessageBox.Show("Please fill all fields.\n(Remember that the email has to contain a '@').", "Can't Signup");
            }
        }

        // Signup request.
        public void SignupRequest(string username, string password, string email)
        {
            SignupStruct request = new SignupStruct
            {
                Username = username,
                Password = password,
                Email = email
            };

            byte[] msg = Serializer.SignupSerializer(request);
            string recv = RequestSender.Send(msg, this.sender);

            SignupResponse ans = JsonConvert.DeserializeObject<SignupResponse>(recv);
            if (ans.Result)
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    Menu menuWindow = new Menu(this.sender);
                    menuWindow.Show();
                    this.Close();
                });
            }
            else
            {
                MessageBox.Show("Signup has failed...", "Error");
            }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow startWindow = new MainWindow();
            startWindow.Show();
            this.Close();
        }
    }
}
