﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        readonly int LOGIN_SUCCESFUL = 1;
        readonly int BAD_INFO = 2;
        readonly int ALREADY_LOGGED = 3;
        private Socket sender;

        public Login(Socket socket)
        {
            InitializeComponent();

            this.sender = socket;
        }

        private void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string username = this.txtUsername.Text;
            string password = this.txtPassword.Password;

            if (username.Length == 0 || password.Length == 0)
            {
                MessageBox.Show("Please fill all fields.", "Can't Login");
                return;
            }
            else
            {
                Thread thread = new Thread(() => LoginRequest(username, password));
                thread.Start();
            }
        }

        // Login request.
        public void LoginRequest(string username, string password)
        {
            LoginStruct request = new LoginStruct
            {
                Username = username,
                Password = password
            };

            byte[] msg = Serializer.LoginSerializer(request);
            string recv = RequestSender.Send(msg, this.sender);

            LoginResponse ans = JsonConvert.DeserializeObject<LoginResponse>(recv);
            if (ans.Result == LOGIN_SUCCESFUL)
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    Menu menuWindow = new Menu(this.sender);
                    menuWindow.Show();
                    this.Close();
                });
            }
            else if(ans.Result == BAD_INFO)
            {
                MessageBox.Show("Wrong password and/or username. Try again.", "Error");
            }
            else
            { // ALREADY_LOGGED
                MessageBox.Show("User is already logged in.", "Error");
            }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow startWindow = new MainWindow();
            startWindow.Show();
            this.Close();
        }
    }
}
