﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_Front_End
{
    // Requests.
    public struct LoginStruct
    {
        public string Username;
        public string Password;
    }

    public struct SignupStruct
    {
        public string Username;
        public string Password;
        public string Email;
    }

    public struct GetPlayersInRoomRequest
    {
        public int Room_Id;
    }

    public struct JoinRoomRequest
    {
        public int Room_Id;
    }

    public struct CreateRoomRequest
    {
        public string Room_Name;
        public string Room_Max;
        public int Room_Count;
        public int Room_Timeout;
    }

    public struct SubmitAnswerRequest
    {
        public uint Answer_Id;
        public float Time;
    }
}
