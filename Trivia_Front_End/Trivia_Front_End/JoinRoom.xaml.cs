﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : Window
    {
        private Socket sender;
        private Dictionary<string, RoomDataResponse> RoomIds;

        public JoinRoom(Socket socket)
        {
            InitializeComponent();
            this.sender = socket;
            this.RoomIds = new Dictionary<string, RoomDataResponse>();

            Thread thread = new Thread(() => Refresh());
            thread.Start();
        }

        public class RoomName
        {
            public string Title { get; set; }

            public RoomName(string str)
            {
                this.Title = str;
            }
        }

        public List<RoomDataResponse> Rooms()
        {
            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_GET_ROOMS;
            string recv = RequestSender.Send(msg, this.sender);

            GetRoomsResponse ans = JsonConvert.DeserializeObject<GetRoomsResponse>(recv);
            return ans.Rooms;
        }

        public void Refresh()
        {
            List<RoomName> RoomNames = new List<RoomName>();
            List<RoomDataResponse> DataRooms = Rooms();
            this.RoomIds.Clear();

            if(DataRooms != null)
            {
                foreach (var data in DataRooms)
                { // can add any data to name.
                    RoomNames.Add(new RoomName(data.Name));
                    this.RoomIds[data.Name] = data;
                }

                this.Dispatcher.Invoke(() =>
                {
                    RoomsList.ItemsSource = RoomNames;
                });
            }
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            Thread thread = new Thread(() => Refresh());
            thread.Start();
        }

        private void btnJoin_Click(object sender, RoutedEventArgs e)
        {
            if (RoomsList.SelectedItem == null)
            {
                MessageBox.Show("Please select a room.", "Can't Join Room");
            }
            else
            {
                RoomDataResponse data = RoomIds[((RoomName)RoomsList.SelectedItem).Title];

                JoinRoomRequest request = new JoinRoomRequest
                {
                    Room_Id = (int)data.Id
                };

                Thread thread = new Thread(() => JoinRequest(request, data));
                thread.Start();
            }
        }

        private void JoinRequest(JoinRoomRequest request, RoomDataResponse data)
        {
            byte[] msg = Serializer.JoinRoomSerializer(request);
            string recv = RequestSender.Send(msg, this.sender);

            JoinRoomResponse ans = JsonConvert.DeserializeObject<JoinRoomResponse>(recv);
            if (ans.Status)
            { // room created
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    RegularRoom regularRoomWindow = new RegularRoom(this.sender, new RoomData(data));
                    regularRoomWindow.Show();
                    this.Close();
                });
            }
            else
            {
                MessageBox.Show("Couldn't join room, please try later...", "Join Error");
            }
        }

        private void btnSignout_Click(object sender, RoutedEventArgs e)
        {
            Menu.Logout(this.sender);
            MainWindow startWindow = new MainWindow();
            startWindow.Show();
            this.Close();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Menu.Logout(this.sender);
            Application.Current.Shutdown();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Menu menuWindow = new Menu(this.sender);
            menuWindow.Show();
            this.Close();
        }
    }
}
