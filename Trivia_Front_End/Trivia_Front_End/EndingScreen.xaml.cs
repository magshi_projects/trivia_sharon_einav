﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for EndingScreen.xaml
    /// </summary>
    public partial class EndingScreen : Window
    {
        private Socket sender;
        private bool _isAdmin;
        private RoomData _room;

        public EndingScreen(Socket socket, bool IsAdmin, RoomData room)
        {
            InitializeComponent();

            this.sender = socket;
            this._isAdmin = IsAdmin;
            this._room = room;

            SetResults(); // Setting the results ListBox.
            SubmitGame(); // Submiting game data.
        }

        public class PlayerScore
        {
            public string Title { get; set; }

            public PlayerScore(string str)
            {
                this.Title = str;
            }
        }

        private void SetResults()
        {
            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_GAME_RESULTS;

            string recv = RequestSender.Send(msg, this.sender);
            GetGameResultsResponse ans = JsonConvert.DeserializeObject<GetGameResultsResponse>(recv);

            List<PlayerScore> results = new List<PlayerScore>();
            if (ans.Status)
            {
                foreach (var result in ans.Results)
                { // Formating all the data of players.
                    results.Add(new PlayerScore(String.Format("{0} -    Correct = {1},  Wrong = {2},  Avg = {3:0.00}",
                        result.Username, result.Correct, result.Wrong, result.Avg)));
                }
                // Inserting all the results.
                Results_List.ItemsSource = results;
            }
        }

        private void SubmitGame()
        { // Can now return to room.
            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_LEAVE_GAME;

            string recv = RequestSender.Send(msg, this.sender);
            LeaveGameResponse ans = JsonConvert.DeserializeObject<LeaveGameResponse>(recv);
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            if(this._isAdmin)
            {
                AdminRoom adminRoomWindow = new AdminRoom(this.sender, this._room);
                adminRoomWindow.Show();
            }
            else
            {
                RegularRoom regularRoomWindow = new RegularRoom(this.sender, this._room);
                regularRoomWindow.Show();
            }
            this.Close();
        }
    }
}
