﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for Highscores.xaml
    /// </summary>
    /// 

    public partial class Highscores : Window
    {
        private Socket sender;

        public Highscores(Socket socket)
        {
            InitializeComponent();
            this.sender = socket;

            List<Player> playerList = new List<Player>();
            Dictionary<string, int> dict = this.Highscore();

            var ordered = dict.OrderByDescending(x => x.Value); // Sorting Players.
            foreach (var item in ordered) // Running from big to small.
            {
                playerList.Add(new Player(item.Key + " - " + item.Value));
            }
            this.PlayerList.ItemsSource = playerList;
        }

        public class Player
        {
            public string Title { get; set; }

            public Player(string str)
            {
                this.Title = str;
            }
        }

        public Dictionary<string, int> Highscore()
        {
            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_HIGHSCORES;
            string recv = RequestSender.Send(msg, this.sender);

            HighscoreResponse ans = JsonConvert.DeserializeObject<HighscoreResponse>(recv);
            return ans.Highscore;
        }

        private void btnSignout_Click(object sender, RoutedEventArgs e)
        {
            if (Menu.Logout(this.sender))
            {
                MainWindow startWindow = new MainWindow(this.sender);
                startWindow.Show();
                this.Close();
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Menu.Logout(this.sender);
            Application.Current.Shutdown();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Menu menuWindow = new Menu(this.sender);
            menuWindow.Show();
            this.Close();
        }
    }
}
