﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        private Socket sender;

        public Menu(Socket socket)
        {
            InitializeComponent();

            this.sender = socket;
        }

        public static bool Logout(Socket sender)
        {
            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_LOGOUT;
            string recv = RequestSender.Send(msg, sender);

            LogoutResponse ans = JsonConvert.DeserializeObject<LogoutResponse>(recv);
            return ans.Status;
        }

        private void btnSignout_Click(object sender, RoutedEventArgs e)
        {
            if(Logout(this.sender))
            {
                MainWindow startWindow = new MainWindow(this.sender);
                startWindow.Show();
                this.Close();
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Logout(this.sender); // Logging out.
            Application.Current.Shutdown();
        }

        private void btnJoinRoom_Click(object sender, RoutedEventArgs e)
        {
            JoinRoom joinRoomWindow = new JoinRoom(this.sender);
            joinRoomWindow.Show();
            this.Close();
        }
       
        private void btnCreateRoom_Click(object sender, RoutedEventArgs e)
        {
            CreateRoom createRoomWindow = new CreateRoom(this.sender);
            createRoomWindow.Show();
            this.Close();
        }

        private void btnMyStatus_Click(object sender, RoutedEventArgs e)
        {
            MyStatus MyStatusWindow = new MyStatus(this.sender);
            MyStatusWindow.Show();
            this.Close();
        }

        private void btnHighscores_Click(object sender, RoutedEventArgs e)
        {
            Highscores HighscoresWindow = new Highscores(this.sender);
            HighscoresWindow.Show();
            this.Close();
        }
    }
}
