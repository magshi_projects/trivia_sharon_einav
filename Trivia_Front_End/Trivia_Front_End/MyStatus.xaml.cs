﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using Newtonsoft.Json;


namespace Trivia_Front_End
{
    /// <summary>
    /// Interaction logic for MyStatus.xaml
    /// </summary>
    public partial class MyStatus : Window
    {
        readonly int startIndex = 0;
        readonly int NumberAfterPoint = 4; // 3 numbers.
        private Socket sender;


        public MyStatus(Socket socket)
        {
            InitializeComponent();
            this.sender = socket;

            byte[] msg = new byte[2];
            msg[0] = (byte)Responses.ID_MY_STATUS;

            string recv = RequestSender.Send(msg, this.sender);
            MyStatusResponse ans = JsonConvert.DeserializeObject<MyStatusResponse>(recv);

            this.Number_Of_Games.Text = ans.Games;
            this.Right_Ans.Text = ans.Right;
            this.Wrong_Ans.Text = ans.Wrong;

            int pointIndex = ans.Avg.IndexOf('.'); // Setting string to 3 number after the point.
            this.Avg_Time.Text = ans.Avg.Substring(startIndex, pointIndex + NumberAfterPoint);
        }

        private void btnSignout_Click(object sender, RoutedEventArgs e)
        {
            Menu.Logout(this.sender);
            MainWindow startWindow = new MainWindow();
            startWindow.Show();
            this.Close();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Menu.Logout(this.sender);
            Application.Current.Shutdown();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Menu menuWindow = new Menu(this.sender);
            menuWindow.Show();
            this.Close();
        }
    }
}
