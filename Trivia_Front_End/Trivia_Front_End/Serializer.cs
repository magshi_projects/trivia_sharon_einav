﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Trivia_Front_End
{
    class Serializer
    {
        public static byte[] dataSerializer(string json, byte msg_type)
        {
            List<byte> buffer = new List<byte>();
            buffer.Add(msg_type);
            buffer.AddRange(Encoding.ASCII.GetBytes(json));

            return buffer.ToArray();
        }

        public static byte[] LoginSerializer(LoginStruct request)
        {
            string json = JsonConvert.SerializeObject(request);
            return dataSerializer(json, (byte)Responses.ID_LOGIN);
        }

        public static byte[] SignupSerializer(SignupStruct request)
        {
            string json = JsonConvert.SerializeObject(request);
            return dataSerializer(json, (byte)Responses.ID_SIGNUP);
        }

        public static byte[] GetPlayersSerializer(GetPlayersInRoomRequest request)
        {
            string json = JsonConvert.SerializeObject(request);
            return dataSerializer(json, (byte)Responses.ID_PLAYERS_IN_ROOM);
        }

        public static byte[] JoinRoomSerializer(JoinRoomRequest request)
        {
            string json = JsonConvert.SerializeObject(request);
            return dataSerializer(json, (byte)Responses.ID_JOIN_ROOM);
        }

        public static byte[] CreateRoomSerializer(CreateRoomRequest request)
        {
            string json = JsonConvert.SerializeObject(request);
            return dataSerializer(json, (byte)Responses.ID_CREATE_ROOM);
        }

        public static byte[] CreateRoomSerializer(SubmitAnswerRequest request)
        {
            string json = JsonConvert.SerializeObject(request);
            return dataSerializer(json, (byte)Responses.ID_SUBMIT_ANSWER);
        }
    }
}
