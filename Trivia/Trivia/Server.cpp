#include "Server.h"


Server::Server(int port) :
	_database(new SqlDatabase()), _handlerFactory(this->_database),
	_io_context() ,_communicator(&_handlerFactory, _io_context, port)
{
}

Server::~Server()
{
	delete this->_database;
	this->_database = nullptr;
}

void Server::run()
{
	this->_communicator.bindAndListen();
	this->_io_context.run();
}