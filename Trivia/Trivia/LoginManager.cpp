#include "LoginManager.h"

// std::vector<LoggedUser> LoginManager::_loggedUsers;

LoginManager::LoginManager(IDatabase* database)
{
	this->_database = database;
}

LoginManager::~LoginManager()
{
}

bool LoginManager::signup(std::string& username, std::string& password, std::string& email)
{
	try
	{
		if (!this->_database->doesUserExist(username)) // checking if user already exists.
		{ // if doesn't exist.
			if (this->validEmail(email)) // checking email valid.
			{
				this->_database->addUser(username, password, email);
				this->_loggedUsers.push_back(LoggedUser(username)); // adding to logged users.
				return true; // signup succesful.
			}
		}
	}
	catch (std::exception& exp)
	{
		std::cout << exp.what() << std::endl;
	}
	return false; // does exist.
}

int LoginManager::login(std::string& username, std::string& password)
{
	try
	{
		if (!this->checkLogged(username)) // checking if user already is logged in.
		{
			if (this->_database->checkInfo(username, password))
			{ // if the info is valid then can login.
				this->_loggedUsers.push_back(LoggedUser(username)); // adding to logged users.
				return LOGIN_SUCCESFUL; // login succesful.
			}
			else
			{
				return BAD_INFO;
			}
		}
	}
	catch (std::exception& exp)
	{
		std::cout << exp.what() << std::endl;
	}
	return ALREADY_LOGGED;
}

void LoginManager::logout()
{
	this->_loggedUsers.clear();
}

bool LoginManager::signout(LoggedUser user)
{
	for (auto it = this->_loggedUsers.begin(); it != this->_loggedUsers.end(); ++it)
	{
		if (it->getUsername() == user.getUsername())
		{
			this->_loggedUsers.erase(it);
			return true; // success.
		}
	}
	return false; // wasn't able to signout the user.
}

bool LoginManager::checkLogged(std::string& username)
{
	for (auto& it : this->_loggedUsers)
	{
		if (it.getUsername() == username)
		{ // found user.
			return true;
		}
	}
	return false; // not found.
}

bool LoginManager::validEmail(std::string& email)
{
	int count = 0;

	for (int i = 0; i < email.size(); i++)
	{
		if (email[i] == '@')
		{
			count++;
		}
	}	
	return count == AT_SIGN_EMAIL;
}