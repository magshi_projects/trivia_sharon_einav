#include "RequestHandlerFactory.h"


RequestHandlerFactory::RequestHandlerFactory(IDatabase* database) : _database(database)
{
	this->_loginManager = new LoginManager(_database);
	this->_highscoretable = new HighscoreTable(_database);
	this->_roomManager = new RoomManager();
	this->_gameManager = new GameManager(_database);
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	delete this->_loginManager;
	delete this->_roomManager;
	delete this->_highscoretable;
	delete this->_gameManager;

	this->_loginManager = nullptr;
	this->_roomManager = nullptr;
	this->_highscoretable = nullptr;
	this->_gameManager = nullptr;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this->_loginManager, this);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return new MenuRequestHandler(user, this->_roomManager, this->_highscoretable, this, this->_loginManager, this->_database);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(boost::shared_ptr<Room> room, LoggedUser user)
{
	return new RoomAdminRequestHandler(room, user, this->_roomManager, this->_gameManager, this);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(boost::shared_ptr<Room> room, LoggedUser user)
{
	return new RoomMemberRequestHandler(room, user, this->_roomManager, this->_gameManager, this);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(
	boost::shared_ptr<Game> game, boost::shared_ptr<Room> room, LoggedUser user, bool isAdmin)
{
	return new GameRequestHandler(game, room, user, this->_gameManager, this, isAdmin);
}