#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager* loginManager, RequestHandlerFactory* handlerFactory);
	~LoginRequestHandler();

	virtual bool isRequestRelevant(Request&);
	virtual RequestResult handleRequest(Request&);


private:
	LoginManager* _loginManager;
	RequestHandlerFactory* _handlerFactory;

	RequestResult login(Request&);
	RequestResult signup(Request&);
};