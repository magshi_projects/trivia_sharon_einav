#pragma once
#include "MyException.h"

class SQLException : public MyException {
public:
	SQLException() : MyException("Error: Can't perform operation, there's been a prblem with SQL server.") {}
};
