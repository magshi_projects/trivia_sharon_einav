#pragma once
#include <ctime>
#include <string>
#include <cstddef>
#include <vector>

#include "JsonRequestPacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

//typedef std::vector<int8_t> Buffer;
typedef int8_t byte;

class IRequestHandler;

struct Request
{
	void updateBuffer(Buffer& holder)
	{
		this->buffer.clear();
		this->buffer.insert(this->buffer.begin(), holder.begin(), holder.end());
	}

	int id;
	std::string receivalTime;
	Buffer buffer;
};

struct RequestResult
{
	Buffer response;
	IRequestHandler* newHandler;
	std::string log;
};

class IRequestHandler
{ // pure virtual class of the request handlers
public:
	IRequestHandler() {};
	~IRequestHandler() {}

	virtual bool isRequestRelevant(Request&) = 0;
	virtual RequestResult handleRequest(Request&) = 0;
};