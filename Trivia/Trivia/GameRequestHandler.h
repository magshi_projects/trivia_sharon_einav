#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "GameManager.h"

class RequestHandlerFactory;

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(boost::shared_ptr<Game> game, boost::shared_ptr<Room> room, 
		LoggedUser user, GameManager* gameManager, RequestHandlerFactory* handlerFactory, bool isAdmin);
	~GameRequestHandler();

	virtual bool isRequestRelevant(Request& request);
	virtual RequestResult handleRequest(Request& request);

private:
	bool _isAdmin;
	boost::shared_ptr<Game> _game;
	boost::shared_ptr<Room> _room;
	LoggedUser _user;
	GameManager* _gameManager;
	RequestHandlerFactory* _handlerFactory;


	RequestResult getQuestion(Request);
	RequestResult submitAnswer(Request);
	RequestResult getGameResults(Request);
	RequestResult leaveGame(Request);
};