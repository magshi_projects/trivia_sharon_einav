#pragma once
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketSerializer.h"
#include "IDatabase.h"
#include "IRequestHandler.h"

#include <boost/asio.hpp> // for sockets.
#include <boost/thread.hpp> // for threads.
#include <boost/asio/thread_pool.hpp> // for thread pool.
#include <boost/make_shared.hpp>
#include <boost/array.hpp>
#include <boost/unordered_map.hpp>

#include <memory>
#include <map>
#include <ctime>
#include <iostream>
#include <iterator>
#include <mutex>
#include <condition_variable>


#define ID_PLACEMENT 0
#define CLIENT_EXIT 88
#define ERROR_IRELEVENT "Error: IRelevent request was made by client."
#define TIME_SLEEP 250
#define NUM_THREADS 5

enum { max_length = 1024 };

using boost::asio::ip::tcp;

class Communicator
{
public:
	Communicator(RequestHandlerFactory* handlerFactory, boost::asio::io_context& io_context, int port);
	~Communicator();

	void bindAndListen();

private:
	RequestHandlerFactory* _handlerFactory;
	boost::unordered_map<boost::shared_ptr<tcp::socket>, IRequestHandler*> _clients;
	tcp::acceptor _acceptor;
	boost::asio::io_context& _io_context;
	bool _running;
	int _port;
	std::mutex _client_lock;
	std::condition_variable _manager_sleep;

	void client_manager();
	void post(boost::shared_ptr<tcp::socket> socket, boost::asio::thread_pool& pool);
	void appendClients(boost::shared_ptr<tcp::socket>& socket);

	static Request recvRequest(boost::shared_ptr<tcp::socket> socket);
	static void sendResponse(boost::shared_ptr<tcp::socket> socket, RequestResult result);
	static void sendError(boost::shared_ptr<tcp::socket> socket, std::string error);
	static Request extractRequest(Buffer& buffer);
	static std::string make_daytime();
};