#include "RoomAdminRequestHandler.h"


RoomAdminRequestHandler::RoomAdminRequestHandler(
	boost::shared_ptr<Room> room, LoggedUser user, RoomManager* roomManager, GameManager* gameManager, RequestHandlerFactory* handlerFactory)
	: _room(room), _user(user), _roomManager(roomManager), _gameManager(gameManager), _handlerFactory(handlerFactory)
{
}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
}

bool RoomAdminRequestHandler::isRequestRelevant(Request& request)
{
	return request.id == ID_CLOSE_ROOM || request.id == ID_START_GAME ||
		request.id == ID_GET_ROOM_STATE;
}

RequestResult RoomAdminRequestHandler::handleRequest(Request& request)
{
	switch (request.id)
	{
	case ID_CLOSE_ROOM:
		return this->closeRoom(request);
		break;
	case ID_START_GAME:
		return this->startGame(request);
		break;
	case ID_GET_ROOM_STATE:
		return this->getRoomState(request);
		break;
	default:
		return RequestResult(); // values == nullptr
		break;
	}
}

RequestResult RoomAdminRequestHandler::closeRoom(Request request)
{
	RequestResult result;

	this->_roomManager->deleteRoom(this->_room->getRoomData()._id); // getting the ID of the room and deleting.

	result.response = JsonRequestPacketSerializer::serializeResponse(
		CloseRoomResponse(true)); // closed room.
	result.newHandler = this->_handlerFactory->createMenuRequestHandler(this->_user); // returns to menu after closing the room.
	result.log = this->_user.getUsername() + " - has closed room: " + this->_room->getRoomData()._name + ".";

	return result;
}

RequestResult RoomAdminRequestHandler::startGame(Request request)
{
	RequestResult result;

	bool activated = this->_room->activate();
	boost::shared_ptr<Game> game = this->_gameManager->createGame(this->_room.get());

	result.response = JsonRequestPacketSerializer::serializeResponse(
		StartGameResponse(activated));
	result.newHandler = this->_handlerFactory->createGameRequestHandler(game, this->_room, this->_user, true); // Moving to game.
	result.log = this->_user.getUsername() + " - succesfuly started game of room: " + this->_room->getRoomData()._name + ".";

	return result;
}

RequestResult RoomAdminRequestHandler::getRoomState(Request request)
{
	RequestResult result;

	result.response = JsonRequestPacketSerializer::serializeResponse(
		GetRoomStateResponse(true, this->_room->getRoomData(), this->_room->getPlayers()));
	result.newHandler = nullptr; // handler doesn't change, still in room.
	//result.log = this->_user.getUsername() + " - has request state of room: " + this->_room->getRoomData()._name + ".";

	return result;
}