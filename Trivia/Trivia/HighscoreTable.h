#pragma once
#include "IDatabase.h"

class HighscoreTable
{
public:
	HighscoreTable(IDatabase* database);
	~HighscoreTable();

	// just calling the method of IDatabase : getHighscores and returns the map.
	std::map<LoggedUser, int> getHighscores();
	
private:
	IDatabase* _database;
};