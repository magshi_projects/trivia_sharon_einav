#include "JsonRequestPacketDeserializer.h"


json deserializeRequest::deserialize(Buffer msg)
{
	uint8_t* data = msg.data();
	//uint32_t dataSize = int((unsigned char)(data[BUFFER_SIZE - 3]) << 24 |
	//	(unsigned char)(data[BUFFER_SIZE - 2]) << 16 |
	//	(unsigned char)(data[BUFFER_SIZE - 1]) << 8 |
	//	(unsigned char)(data[BUFFER_SIZE]));
	uint32_t dataSize = msg.size() - MSG_TYPE;

	uint8_t * dataJson = new uint8_t[dataSize];
	memcpy(dataJson, data + MSG_TYPE, dataSize); //getting json part from data according to size

	Buffer jsonBuff(dataJson, dataJson + dataSize);

	delete[] dataJson; //freeing memory
	return json::parse(jsonBuff); //turning Buffer to json
}

LoginRequest deserializeRequest::deserializeLoginRequest(Buffer msg)
{
	LoginRequest data;
	json dumpJson = deserialize(msg);

	//putting information from json into struct
	dumpJson[PASSWORD].get_to(data.password);
	dumpJson[USERNAME].get_to(data.username);
	return data;
}

SignupRequest deserializeRequest::deserializeSignupRequest(Buffer msg)
{
	SignupRequest data;
	json dumpJson = deserialize(msg);

	//putting information from json into struct
	dumpJson[PASSWORD].get_to(data.password);
	dumpJson[USERNAME].get_to(data.username);
	dumpJson[EMAIL].get_to(data.email);
	return data;
}

GetPlayersInRoomRequest deserializeRequest::deserializeGetPlayersRequest(Buffer msg)
{
	GetPlayersInRoomRequest data;
	json dumpJson = deserialize(msg);

	// getting room id to see how many players in.
	dumpJson[ROOM_ID].get_to(data.roomId);
	return data;
}

JoinRoomRequest deserializeRequest::deserializeJoinRoomRequest(Buffer msg)
{
	JoinRoomRequest data;
	json dumpJson = deserialize(msg);

	// getting room id the user requests to join.
	dumpJson[ROOM_ID].get_to(data.roomId);
	return data;
}

CreateRoomRequest deserializeRequest::deserializeCreateRoomRequest(Buffer msg)
{
	CreateRoomRequest data;
	json dumpJson = deserialize(msg);

	// getting the info on the room user desires to create.
	dumpJson[ROOM_NAME].get_to(data.roomName);
	dumpJson[ROOM_MAX].get_to(data.maxUsers);
	dumpJson[ROOM_COUNT].get_to(data.questionCount);
	dumpJson[ROOM_TIMEOUT].get_to(data.answerTimeout);

	return data;
}

SubmitAnswerRequest deserializeRequest::deserializeSubmitAnswerRequest(Buffer msg)
{
	SubmitAnswerRequest data;
	json dumpJson = deserialize(msg);

	// getting answer info, time to answer and id.
	dumpJson[ANSWER_ID].get_to(data.answerId);
	dumpJson[TIME].get_to(data.time);

	return data;
}