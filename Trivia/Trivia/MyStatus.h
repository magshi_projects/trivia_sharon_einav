#pragma once
#include <string>

class MyStatus
{
public:
	MyStatus()
	{
	}

	MyStatus(std::string games, std::string right, std::string wrong, std::string avg) :
		_games(games), _right(right), _wrong(wrong), _avg(avg)
	{
	}

	~MyStatus()
	{
	}

	std::string _games;
	std::string _right;
	std::string _wrong;
	std::string _avg;
};