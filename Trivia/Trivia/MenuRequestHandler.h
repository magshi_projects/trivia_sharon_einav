#pragma once
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "IRequestHandler.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "HighscoreTable.h"

class RequestHandlerFactory;
class RoomManager;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(LoggedUser user, RoomManager* roomManager, HighscoreTable* highscoreTable,
		RequestHandlerFactory* handlerFactory, LoginManager* loginManager, IDatabase* database);
	~MenuRequestHandler();

	virtual bool isRequestRelevant(Request&);
	virtual RequestResult handleRequest(Request&);

private:
	LoggedUser _user;
	RoomManager* _roomManager;
	HighscoreTable* _highscoreTable;
	RequestHandlerFactory* _handlerFactory;
	LoginManager* _loginManager;
	IDatabase* _database;

	RequestResult getRooms(Request);
	RequestResult getPlayersInRoom(Request);
	RequestResult getHighscores(Request);
	RequestResult getStatus(Request);

	RequestResult signout(Request);
	RequestResult joinRoom(Request);
	RequestResult createRoom(Request);
};