#pragma once
#include "LoggedUser.h"
#include "IDatabase.h"

#include <vector>
#include <iostream>

#define AT_SIGN_EMAIL 1
#define LOGIN_SUCCESFUL 1
#define BAD_INFO 2
#define ALREADY_LOGGED 3

class LoginManager
{
public:
	LoginManager(IDatabase* database);
	~LoginManager();

	bool signup(std::string& username, std::string& password, std::string& email);
	int login(std::string& username, std::string& password);
	void logout();
	bool signout(LoggedUser);

private:
	IDatabase* _database;
	std::vector<LoggedUser> _loggedUsers;

	bool checkLogged(std::string& username);
	bool validEmail(std::string& email);
};