#pragma once
#include <string>
#include <vector>
#include <algorithm>

class Question
{
public:
	Question() {}
	Question(std::string question, std::string correct, std::string ans2, std::string ans3, std::string ans4);
	Question(const Question& question);
	~Question();

	std::string getQuestion();
	std::string getCorrectAnswer();
	std::vector<std::string> getPossibleAnswers();

	unsigned int idCorrect();

private:
	std::string _question;
	std::string _correct;
	std::vector<std::string> _possibleAnswers;
};