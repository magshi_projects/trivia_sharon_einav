#pragma once
#include <string>
#include <vector>
#include <map>

#include "LoggedUser.h"
#include "Room.h"

#define RESULT "Result"
#define MESSAGE "Message"
#define STATUS "Status"
#define ROOMS "Rooms"
#define HIGHSCORE "Highscore"
#define QUESTION_COUNT "QuestionCount"
#define TIMEOUT "Timeout"
#define HAS_BEGUN "HasBegun"
#define PLAYERS "Players"
#define CORRECT_ID "CorrectId"
#define QUESTION "Question"
#define ANSWERS "Answers"
#define RESULTS "Results"
#define GAMES "Games"
#define RIGHT "Right"
#define WRONG "Wrong"
#define AVG "Avg"

enum Responses {
	ID_ERR, ID_LOGIN, ID_SIGNUP, // Login/Signup.
	ID_LOGOUT, ID_GET_ROOMS, ID_PLAYERS_IN_ROOM, // Menu.
	ID_JOIN_ROOM, ID_CREATE_ROOM, ID_HIGHSCORES, ID_MY_STATUS,
	ID_CLOSE_ROOM, ID_START_GAME, ID_GET_ROOM_STATE, // Room.
	ID_LEAVE_ROOM,
	ID_GET_QUESTION, ID_SUBMIT_ANSWER, ID_GAME_RESULTS, ID_LEAVE_GAME // Game
};

struct CloseRoomResponse
{
	CloseRoomResponse(unsigned int ResponseStatus)
	{
		status = ResponseStatus;
	}
	unsigned int status;
};

struct StartGameResponse
{
	StartGameResponse(unsigned int ResponseStatus)
	{
		status = ResponseStatus;
	}
	unsigned int status;
};

struct GetRoomStateResponse
{
	GetRoomStateResponse(unsigned int _status, RoomData data, std::vector<LoggedUser> roomPlayers)
	{
		status = _status;
		questionCount = data._questionCount;
		answerTimeout = data._timePerQuestion;
		hasGameBegun = data._isActive;
		for (auto& it : roomPlayers)
		{ // copying all the usernames in room.
			players.push_back(it.getUsername());
		}
	}
	GetRoomStateResponse(unsigned int _status)
	{
		status = _status;
	}

	unsigned int status;
	unsigned int questionCount;
	unsigned int answerTimeout;
	bool hasGameBegun;
	std::vector<std::string> players;
};

struct LeaveRoomResponse
{
	LeaveRoomResponse(unsigned int ResponseStatus)
	{
		status = ResponseStatus;
	}
	unsigned int status;
};

struct LogoutResponse
{
	LogoutResponse(unsigned int ResponseStatus)
	{
		status = ResponseStatus;
	}
	unsigned int status;
};

struct GetRoomsResponse
{
	GetRoomsResponse() {}
	GetRoomsResponse(unsigned int ResponseStatus, std::vector<RoomData> ResponseRooms)
	{
		status = ResponseStatus;
		rooms = ResponseRooms;
	}
	unsigned int status;
	std::vector<RoomData> rooms;
};

struct GetPlayersInRoomResponse
{
	GetPlayersInRoomResponse() {}
	GetPlayersInRoomResponse(std::vector<LoggedUser> room_players)
	{
		roomPlayers = room_players;
	}
	std::vector<LoggedUser> roomPlayers;
};

struct HighscoreResponse
{
	HighscoreResponse() {}
	HighscoreResponse(unsigned int ResponseStatus, std::map<LoggedUser, int> ResponseHighscores)
	{
		status = ResponseStatus;
		highscores = ResponseHighscores;
	}
	unsigned int status;
	std::map<LoggedUser, int> highscores;
};

struct JoinRoomResponse
{
	JoinRoomResponse() {}
	JoinRoomResponse(unsigned int ResponseStatus)
	{
		status = ResponseStatus;
	}
	unsigned int status;
};

struct CreateRoomResponse
{
	CreateRoomResponse() {}
	CreateRoomResponse(unsigned int ResponseStatus)
	{
		status = ResponseStatus;
	}
	unsigned int status;
};

struct MyStatusResponse
{
	MyStatusResponse() {}
	MyStatusResponse(std::string games, std::string right, std::string wrong, std::string avg) :
		Games(games), Right(right), Wrong(wrong), Avg(avg)
	{
	}

	std::string Games;
	std::string Right;
	std::string Wrong;
	std::string Avg;
};

struct ErrorResponse
{
	ErrorResponse() {}
	ErrorResponse(std::string msg)
	{
		errMsg = msg;
	}
	std::string errMsg;
};

struct LoginResponse
{
	LoginResponse() {}
	LoginResponse(int respone)
	{
		result = respone;
	}
	int result;
};

struct SignupResponse
{
	SignupResponse() {}
	SignupResponse(bool respone)
	{
		result = respone;
	}
	bool result;
};

struct GetQuestionResponse
{
	GetQuestionResponse() {}
	GetQuestionResponse(unsigned int ResponseStatus, std::string ques, std::vector<std::string> ResponseAnswers)
	{
		status = ResponseStatus;
		question = ques;
		answers = ResponseAnswers;
	}
	unsigned int status;
	std::string question;
	std::vector<std::string> answers;
};

struct SubmitAnswerResponse
{
	SubmitAnswerResponse() {}
	SubmitAnswerResponse(unsigned int ResponseStatus, unsigned int correctId)
	{
		status = ResponseStatus;
		correctAnswerId = correctId;
	}
	unsigned int status;
	unsigned int correctAnswerId;
};

struct LeaveGameResponse
{
	LeaveGameResponse(unsigned int ResponseStatus)
	{
		status = ResponseStatus;
	}
	unsigned int status;
};

struct PlayerResults
{
	PlayerResults() {}
	PlayerResults(std::string user, unsigned int correct, unsigned int wrong, float avg)
	{
		username = user;
		correctAnswerCount = correct;
		wrongAnswerCount = wrong;
		averageAnswerTime = avg;
	}

	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;
};

struct GetGameResultsResponse
{
	GetGameResultsResponse() {}
	GetGameResultsResponse(unsigned int ResponseStatus, std::vector<PlayerResults> playerResults)
	{
		status = ResponseStatus;
		results = playerResults;
	}
	unsigned int status;
	std::vector<PlayerResults> results;
};