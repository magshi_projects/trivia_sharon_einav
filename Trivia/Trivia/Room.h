#pragma once
#include <vector>
#include <string>
#include <algorithm>

#include "LoggedUser.h"

struct RoomData
{
	RoomData() // default.
	{ 
	}
	RoomData(unsigned int id, std::string name, std::string maxPlayers, unsigned int timePerQuestion, unsigned int isActive, unsigned int questionCount) :
		_id(id), _name(name), _maxPlayers(maxPlayers), _timePerQuestion(timePerQuestion), _isActive(isActive), _questionCount(questionCount)
	{
	}
	RoomData(const RoomData& other) : // copy.
		_id(other._id), _name(other._name), _maxPlayers(other._maxPlayers),
		_timePerQuestion(other._timePerQuestion), _isActive(other._isActive),
		_questionCount(other._questionCount)
	{ 
	}

	unsigned int _id;
	std::string _name;
	std::string _maxPlayers;
	unsigned int _timePerQuestion;
	unsigned int _questionCount;
	unsigned int _isActive;
};

class Room
{
public:
	Room(); 
	Room(unsigned int id, std::string name, std::string maxPlayers, 
		unsigned int timePerQuestion, unsigned int isActive, unsigned int questionCount);
	Room(const Room& other);
	Room(Room* other);
	~Room();
	void addUser(LoggedUser user);
	void removeUser(LoggedUser user);

	RoomData getRoomData();
	std::vector<LoggedUser> getPlayers();
	int getNumOfUsers();
	unsigned int getId();

	void deactived();
	bool activate();
	bool isFull();

	friend bool operator< (const Room& l, const Room& r)
	{
		return l._metadata._id < r._metadata._id;
	}

private:
	RoomData _metadata;
	std::vector<LoggedUser> _users;
};