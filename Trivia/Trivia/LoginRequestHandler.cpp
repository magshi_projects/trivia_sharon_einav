#include "LoginRequestHandler.h"


LoginRequestHandler::LoginRequestHandler(LoginManager* loginManager, RequestHandlerFactory* handlerFactory) : IRequestHandler()
{
	this->_loginManager = loginManager;
	this->_handlerFactory = handlerFactory;
}

LoginRequestHandler::~LoginRequestHandler()
{ // no deleting because it's a singleton.
}


bool LoginRequestHandler::isRequestRelevant(Request& request)
{
	return request.id == ID_LOGIN || request.id == ID_SIGNUP;
}

RequestResult LoginRequestHandler::handleRequest(Request& request)
{
	if (request.id == ID_LOGIN)
	{
		return this->login(request);
	}
	else
	{
		return this->signup(request);
	}
}


RequestResult LoginRequestHandler::login(Request& request)
{
	// deserializer of request buffer. and getting info.
	LoginRequest info(deserializeRequest::deserializeLoginRequest(request.buffer));
	RequestResult result;

	int answer = this->_loginManager->login(info.username, info.password);

	if (answer == LOGIN_SUCCESFUL)
	{
		result.response = JsonRequestPacketSerializer::serializeResponse(LoginResponse(LOGIN_SUCCESFUL));
		result.newHandler = (IRequestHandler*)this->_handlerFactory->createMenuRequestHandler(LoggedUser(info.username));
		result.log = "New user has logged in - " + info.username + ".";
	}
	else
	{
		result.response = JsonRequestPacketSerializer::serializeResponse(LoginResponse(answer));
		result.newHandler = nullptr;
		result.log = "Client failed to login.";
	}
	
	return result;
}

RequestResult LoginRequestHandler::signup(Request& request)
{
	// deserializer of request buffer.
	SignupRequest info(deserializeRequest::deserializeSignupRequest(request.buffer));
	RequestResult result;

	if (this->_loginManager->signup(info.username, info.password, info.email))
	{
		result.response = JsonRequestPacketSerializer::serializeResponse(SignupResponse(true));
		result.newHandler = (IRequestHandler*)this->_handlerFactory->createMenuRequestHandler(LoggedUser(info.username));
		result.log = "A new user was created - " + info.username + ".";
	}
	else
	{
		result.response = JsonRequestPacketSerializer::serializeResponse(SignupResponse(false));
		result.newHandler = nullptr;
		result.log = "Client failed to signup with user - " + info.username + ".";
	}
	return result;
}