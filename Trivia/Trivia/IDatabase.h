#pragma once
#include <string>
#include <map>
#include <vector>

#include "LoggedUser.h"
#include "Question.h"
#include "Game.h"
#include "MyStatus.h"

struct GameData;

class IDatabase
{
public:

	//General
	virtual ~IDatabase() = default;

	//User table
	virtual void createUserTable() = 0;
	virtual void addUser(std::string username, std::string password, std::string email) = 0;
	virtual void deleteUser(std::string username) = 0;
	virtual bool doesUserExist(std::string username) = 0;
	virtual void clearUserTable() = 0;
	virtual bool checkInfo(std::string username, std::string password) = 0;
	virtual std::map<LoggedUser, int> getHighscores() = 0;
	virtual std::vector<Question> getQuestions(unsigned int amount) = 0;
	virtual void updatePlayer(LoggedUser user, GameData data) = 0;
	virtual MyStatus getStatus(LoggedUser user) = 0;
};