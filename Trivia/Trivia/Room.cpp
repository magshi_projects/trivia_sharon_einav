#include "Room.h"
#include <iostream>

Room::Room()
{
}

Room::Room(unsigned int id, std::string name, std::string maxPlayers, unsigned int timePerQuestion, unsigned int isActive, unsigned int questionCount) :
	_metadata(id, name, maxPlayers, timePerQuestion, isActive, questionCount)
{
}

Room::Room(const Room& other) :
	_metadata(other._metadata), _users(other._users)
{
}

Room::Room(Room* other) :
	_metadata(other->_metadata), _users(other->_users)
{
}

Room::~Room()
{
}

void Room::addUser(LoggedUser user)
{
	this->_users.push_back(user);
}

void Room::removeUser(LoggedUser user)
{
	int count = 0;
	bool deleted = false;
	std::vector<LoggedUser>::iterator it = find(this->_users.begin(), this->_users.end(), user);
	if (it == this->_users.end()) //if didnt find the item
	{
		//throw exception?
		std::cout << "User not found" << std::endl;
	}
	else
	{
		this->_users.erase(it);
	}
}

RoomData Room::getRoomData()
{
	return this->_metadata;
}

std::vector<LoggedUser> Room::getPlayers()
{
	return this->_users;
}

int Room::getNumOfUsers()
{
	return this->_users.size();
}

unsigned int Room::getId()
{
	return this->_metadata._id;
}

void Room::deactived()
{
	this->_metadata._isActive = false;
}

bool Room::activate()
{
	if (this->_metadata._isActive)
	{ // active.
		return false; // already in game.
	}
	this->_metadata._isActive = true;
	return true; // activated.
}

bool Room::isFull()
{
	return std::stoi(this->_metadata._maxPlayers) == this->_users.size();
}