#include "GameRequestHandler.h"


GameRequestHandler::GameRequestHandler(boost::shared_ptr<Game> game, boost::shared_ptr<Room> room, 
	LoggedUser user, GameManager* gameManager, RequestHandlerFactory* handlerFactory, bool isAdmin) :
	_game(game), _room(room), _user(user), _gameManager(gameManager), _handlerFactory(handlerFactory), _isAdmin(isAdmin)
{
}

GameRequestHandler::~GameRequestHandler()
{
}

bool GameRequestHandler::isRequestRelevant(Request& request)
{
	return request.id == ID_GET_QUESTION || request.id == ID_SUBMIT_ANSWER ||
		request.id == ID_GAME_RESULTS || request.id == ID_LEAVE_GAME;
}

RequestResult GameRequestHandler::handleRequest(Request& request)
{
	switch (request.id)
	{
	case ID_GET_QUESTION:
		return this->getQuestion(request);
		break;
	case ID_SUBMIT_ANSWER:
		return this->submitAnswer(request);
		break;
	case ID_GAME_RESULTS:
		return this->getGameResults(request);
		break;
	case ID_LEAVE_GAME:
		return this->leaveGame(request);
		break;
	default:
		return RequestResult(); // values == nullptr
		break;
	}
}

RequestResult GameRequestHandler::getQuestion(Request request)
{
	RequestResult result;

	Question currentQues = this->_game->getQuestionForUser(this->_user);
	result.response = JsonRequestPacketSerializer::serializeResponse(
		GetQuestionResponse(true, currentQues.getQuestion(), currentQues.getPossibleAnswers()));
	result.newHandler = nullptr; // Still in game.
	result.log = this->_user.getUsername() + " - has requested the next question in game.";

	return result;
}

RequestResult GameRequestHandler::submitAnswer(Request request)
{
	SubmitAnswerRequest info(deserializeRequest::deserializeSubmitAnswerRequest(request.buffer));
	RequestResult result;

	unsigned int correctId = this->_game->submitAnswer(this->_user, info.answerId, info.time);

	result.response = JsonRequestPacketSerializer::serializeResponse(
		SubmitAnswerResponse(true, correctId)); // Sending the correct id.
	result.newHandler = nullptr; // Still in game.
	result.log = this->_user.getUsername() + " - has submited an answer: " + std::to_string(info.answerId) + "(ID).";

	return result;
}

RequestResult GameRequestHandler::getGameResults(Request request)
{
	RequestResult result;
	
	std::map<LoggedUser, GameData> results = this->_game->getResults();
	std::vector<PlayerResults> saver;

	for (auto& it : results)
	{ // Adding to vector the player data.
		saver.push_back(PlayerResults(it.first.getUsername(), it.second.correctAnswerCount,
			it.second.wrongAnswerCount, it.second.averageAnswerTime));
	}

	result.response = JsonRequestPacketSerializer::serializeResponse(
		GetGameResultsResponse(true, saver));
	result.newHandler = nullptr; // Still in game.
	result.log = this->_user.getUsername() + " - has request result of game, room: " + this->_room->getRoomData()._name + ".";

	return result;
}

RequestResult GameRequestHandler::leaveGame(Request request)
{
	RequestResult result;

	this->_game->removePlayer(this->_user); // Updating the user's data.
	this->_room->deactived(); // Deactiving when first player finishes.

	result.response = JsonRequestPacketSerializer::serializeResponse(
		LeaveGameResponse(true)); // Can leave.
	if (this->_isAdmin)
	{ // Return to room, is the Admin.
		result.newHandler = this->_handlerFactory->createRoomAdminRequestHandler(this->_room, this->_user);
		result.log = this->_user.getUsername() + " - exited game and returned to room: " + this->_room->getRoomData()._name + ", as Admin.";
	}
	else
	{ // Is member.
		result.newHandler = this->_handlerFactory->createRoomMemberRequestHandler(this->_room, this->_user);
		result.log = this->_user.getUsername() + " - exited game and returned to room: " + this->_room->getRoomData()._name + ", as Member.";
	}

	return result;
}