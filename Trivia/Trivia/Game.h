#pragma once
#include <map>

#include "Question.h"
#include "LoggedUser.h"
#include "IDatabase.h"

class IDatabase;

struct  GameData
{
	Question currentQuestion;
	unsigned int idQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;

	GameData() : idQuestion(0), correctAnswerCount(0), wrongAnswerCount(0), averageAnswerTime(0)
	{
	}

	GameData(const GameData& other)
	{
		this->wrongAnswerCount = other.wrongAnswerCount;
		this->averageAnswerTime = other.averageAnswerTime;
		this->correctAnswerCount = other.correctAnswerCount;
		this->currentQuestion = other.currentQuestion;
		this->idQuestion = other.idQuestion;
	}
};

class Game
{
public:
	Game(IDatabase* database, std::vector<LoggedUser> players, std::vector<Question> questions);
	Game(const Game& other);
	Game(Game* other);
	~Game();

	Question getQuestionForUser(LoggedUser user);
	unsigned int submitAnswer(LoggedUser user, unsigned int idAns, float time);
	void removePlayer(LoggedUser user);

	std::map<LoggedUser, GameData> getResults();

private:
	std::vector<Question> _questions;
	std::map<LoggedUser, GameData> _players;
	IDatabase* _database;
};