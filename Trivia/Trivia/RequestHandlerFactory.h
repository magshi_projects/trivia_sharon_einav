#pragma once
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"

#include "IDatabase.h"
#include "HighscoreTable.h"

#include "RoomManager.h"
#include "LoginManager.h"
#include "GameManager.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase* database);
	~RequestHandlerFactory();

	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(boost::shared_ptr<Room> room, LoggedUser user);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(boost::shared_ptr<Room> room, LoggedUser user);
	GameRequestHandler* createGameRequestHandler(boost::shared_ptr<Game> game, boost::shared_ptr<Room> room, LoggedUser user, bool isAdmin);

private:
	LoginManager* _loginManager;
	RoomManager* _roomManager;
	GameManager* _gameManager;
	HighscoreTable* _highscoretable;
	IDatabase* _database;
};