#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string username)
{
	this->_username = username;
}

LoggedUser::~LoggedUser()
{
}

std::string LoggedUser::getUsername()
{
	return this->_username;
}

std::string LoggedUser::getUsername() const
{
	return this->_username;
}

void LoggedUser::setUsername(std::string name)
{
	this->_username = name;
}


bool LoggedUser::operator<(const LoggedUser& obj) const
{
	return this->_username < obj._username;
}

bool LoggedUser::operator==(const LoggedUser& obj) const
{
	return this->_username == obj._username;
}