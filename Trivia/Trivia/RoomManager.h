#pragma once
#include <map>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "RequestHandlerFactory.h" // for friend.

#include "Room.h"

#define NO_SPACE 0
#define STARTING_ID 1
#define MAX_ROOMS 20

class MenuRequestHandler; // may need changes.
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;

class RoomManager
{
public:
	RoomManager();
	~RoomManager();

	boost::shared_ptr<Room> createRoom(LoggedUser creator, std::string name, std::string maxPlayers, unsigned int timePerQuestion, unsigned int questionCount);
	void deleteRoom(unsigned int roomId);
	unsigned int getRoomState(int ID);

	std::vector<RoomData> getRooms();
	boost::shared_ptr<Room> getRoom(unsigned int id);

private:
	std::map<unsigned int, boost::shared_ptr<Room>> _rooms; // if not working, turn it to public.
	std::map<int, bool> _availableIDs;

	friend MenuRequestHandler;
	friend RoomAdminRequestHandler;
	friend RoomMemberRequestHandler;
};