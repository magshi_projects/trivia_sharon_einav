#include "GameManager.h"


GameManager::GameManager(IDatabase* database)
{
	this->_database = database;
}


GameManager::~GameManager()
{
}

boost::shared_ptr<Game> GameManager::createGame(Room room)
{
	this->_games[room] = boost::make_shared<Game>(
		new Game(this->_database, room.getPlayers(), 
			this->_database->getQuestions(room.getRoomData()._questionCount)));
	return this->_games[room];
}

void GameManager::deleteGame(Room room)
{
	this->_games.erase(room);
}

boost::shared_ptr<Game> GameManager::getGame(Room room)
{
	return this->_games[room];
}