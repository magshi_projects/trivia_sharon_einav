#pragma once
#include <string>
#include "LoggedUser.h"

// Login/Signup
#define PASSWORD "Password"
#define USERNAME "Username"
#define EMAIL "Email"
#define USER "User"

// Menu
#define ROOM_ID "Room_Id"
#define ROOM_NAME "Room_Name"
#define ROOM_MAX "Room_Max"
#define ROOM_COUNT "Room_Count"
#define ROOM_TIMEOUT "Room_Timeout"

// Game
#define ANSWER_ID "Answer_Id"
#define TIME "Time"

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string roomName;
	std::string maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct SignoutRequest
{
	SignoutRequest() : user("")
	{
	}
	LoggedUser user;
};

struct SubmitAnswerRequest
{
	unsigned int answerId;
	float time;
};