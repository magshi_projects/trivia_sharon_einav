#include "HighscoreTable.h"


HighscoreTable::HighscoreTable(IDatabase* database) : _database(database)
{
}

HighscoreTable::~HighscoreTable()
{
}

std::map<LoggedUser, int> HighscoreTable::getHighscores()
{
	return this->_database->getHighscores();
}