#include "Question.h"


Question::Question(std::string question, std::string correct, std::string ans2, std::string ans3, std::string ans4)
{
	this->_question = question;
	this->_correct = correct;

	// pushing answers to the vector.
	this->_possibleAnswers.push_back(correct);
	this->_possibleAnswers.push_back(ans2);
	this->_possibleAnswers.push_back(ans3);
	this->_possibleAnswers.push_back(ans4);

	// shuffling answers.
	std::random_shuffle(this->_possibleAnswers.begin(), this->_possibleAnswers.end());
}

Question::Question(const Question& other)
{
	this->_correct = other._correct;
	this->_possibleAnswers = other._possibleAnswers;
	this->_question = other._question;
}

Question::~Question()
{
}


std::string Question::getQuestion()
{
	return this->_question;
}

std::vector<std::string> Question::getPossibleAnswers()
{
	return this->_possibleAnswers;
}

std::string Question::getCorrectAnswer()
{
	return this->_correct;
}

unsigned int Question::idCorrect()
{
	unsigned int index;
	for (int i = 0; i < this->_possibleAnswers.size(); i++)
	{
		if (this->_possibleAnswers[i] == this->_correct)
		{
			index = i;
		}
	}
	return index;
}