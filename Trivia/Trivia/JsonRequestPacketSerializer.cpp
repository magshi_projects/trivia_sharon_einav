#include "JsonRequestPacketSerializer.h"

// for quick serialization.
void from_json(const json& j, RoomData& val);
void to_json(json& j, const RoomData& val);

void from_json(const json& j, LoggedUser& val);
void to_json(json& j, const LoggedUser& val);

void from_json(const json& j, PlayerResults& val);
void to_json(json& j, const PlayerResults& val);

/*
Function turns json to full msg (message type, message size, json) in bytes
Input: JSON full message, message type
Output: full message Buffer
*/
Buffer JsonRequestPacketSerializer::serialize(json msg, uint8_t msgType)
{
	std::string json = msg.dump();
	Buffer BufferMsg(json.begin(), json.end()); //turning json to bytes.

	BufferMsg.insert(BufferMsg.begin(), msgType); //adding message type to beggning of bson vector

	return BufferMsg;
}

Buffer JsonRequestPacketSerializer::serializeResponse(ErrorResponse error)
{
	json JsonErrMsg;
	JsonErrMsg[MESSAGE] = error.errMsg;

	return serialize(JsonErrMsg, ID_ERR);
}


Buffer JsonRequestPacketSerializer::serializeResponse(LoginResponse response)
{
	json jsonMsg;
	jsonMsg[RESULT] = response.result;
	return serialize(jsonMsg, ID_LOGIN);
}

Buffer JsonRequestPacketSerializer::serializeResponse(SignupResponse response)
{
	json jsonMsg;
	jsonMsg[RESULT] = response.result;
	return serialize(jsonMsg, ID_SIGNUP);
}

Buffer JsonRequestPacketSerializer::serializeResponse(LogoutResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	return serialize(jsonMsg, ID_LOGOUT);
}

Buffer JsonRequestPacketSerializer::serializeResponse(GetRoomsResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	jsonMsg[ROOMS] = response.rooms;
	return serialize(jsonMsg, ID_GET_ROOMS);
}

Buffer JsonRequestPacketSerializer::serializeResponse(JoinRoomResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	return serialize(jsonMsg, ID_JOIN_ROOM);
}

Buffer JsonRequestPacketSerializer::serializeResponse(CreateRoomResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	return serialize(jsonMsg, ID_CREATE_ROOM);
}

Buffer JsonRequestPacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
	json jsonMsg;
	jsonMsg[ROOMS] = response.roomPlayers;
	return serialize(jsonMsg, ID_PLAYERS_IN_ROOM);
}

Buffer JsonRequestPacketSerializer::serializeResponse(HighscoreResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;

	std::map<std::string, int> newHighScore;
	for (auto& it : response.highscores)
	{
		newHighScore[it.first.getUsername()] = it.second;
	}
	jsonMsg[HIGHSCORE] = newHighScore;
	return serialize(jsonMsg, ID_HIGHSCORES);
}

Buffer JsonRequestPacketSerializer::serializeResponse(CloseRoomResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	return serialize(jsonMsg, ID_CLOSE_ROOM);
}

Buffer JsonRequestPacketSerializer::serializeResponse(StartGameResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	return serialize(jsonMsg, ID_START_GAME);
}

Buffer JsonRequestPacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	jsonMsg[QUESTION_COUNT] = response.questionCount;
	jsonMsg[TIMEOUT] = response.answerTimeout;
	jsonMsg[HAS_BEGUN] = response.hasGameBegun;
	jsonMsg[PLAYERS] = response.players;
	return serialize(jsonMsg, ID_GET_ROOM_STATE);
}

Buffer JsonRequestPacketSerializer::serializeResponse(LeaveRoomResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	return serialize(jsonMsg, ID_LEAVE_ROOM);
}

Buffer JsonRequestPacketSerializer::serializeResponse(MyStatusResponse response)
{
	json jsonMsg;
	jsonMsg[GAMES] = response.Games;
	jsonMsg[RIGHT] = response.Right;
	jsonMsg[WRONG] = response.Wrong;
	jsonMsg[AVG] = response.Avg;
	return serialize(jsonMsg, ID_MY_STATUS);
}

Buffer JsonRequestPacketSerializer::serializeResponse(GetQuestionResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	jsonMsg[QUESTION] = response.question;
	jsonMsg[ANSWERS] = response.answers;
	return serialize(jsonMsg, ID_GET_QUESTION);
}

Buffer JsonRequestPacketSerializer::serializeResponse(SubmitAnswerResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	jsonMsg[CORRECT_ID] = response.correctAnswerId;
	return serialize(jsonMsg, ID_SUBMIT_ANSWER);
}

Buffer JsonRequestPacketSerializer::serializeResponse(GetGameResultsResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	jsonMsg[RESULTS] = response.results;
	return serialize(jsonMsg, ID_GAME_RESULTS);
}

Buffer JsonRequestPacketSerializer::serializeResponse(LeaveGameResponse response)
{
	json jsonMsg;
	jsonMsg[STATUS] = response.status;
	return serialize(jsonMsg, ID_LEAVE_GAME);
}

// for the vector<room> serialization.
void from_json(const json& j, RoomData& val)
{
	j.at("Id").get_to(val._id);
	j.at("Name").get_to(val._name);
	j.at("MaxPlayers").get_to(val._maxPlayers);
	j.at("QuestionCount").get_to(val._questionCount);
	j.at("TimePerQuestion").get_to(val._timePerQuestion);
	j.at("IsActive").get_to(val._isActive);
}

void to_json(json& j, const RoomData& val)
{
	j["Id"] = val._id;
	j["Name"] = val._name;
	j["MaxPlayers"] = val._maxPlayers;
	j["QuestionCount"] = val._questionCount;
	j["TimePerQuestion"] = val._timePerQuestion;
	j["IsActive"] = val._isActive;
}

// for the vector<LoggedUser> serialization.
void from_json(const json& j, LoggedUser& val)
{
	val.setUsername(j.at("Username"));
}

void to_json(json& j, const LoggedUser& val)
{
	j["Username"] = val.getUsername();
}

void from_json(const json& j, PlayerResults& val)
{
	j.at("Username").get_to(val.username);
	j.at("Correct").get_to(val.correctAnswerCount);
	j.at("Wrong").get_to(val.wrongAnswerCount);
	j.at("Avg").get_to(val.averageAnswerTime);
}

void to_json(json& j, const PlayerResults& val)
{
	j["Username"] = val.username;
	j["Correct"] = val.correctAnswerCount;
	j["Wrong"] = val.wrongAnswerCount;
	j["Avg"] = val.averageAnswerTime;
}