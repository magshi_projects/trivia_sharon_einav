#include "Game.h"


Game::Game(IDatabase* database, std::vector<LoggedUser> players, std::vector<Question> questions)
{
	this->_database = database;
	this->_questions = questions;

	for (auto& it : players)
	{
		this->_players[it] = GameData();
	}
}

Game::Game(const Game& other)
{
	this->_database = other._database;
	this->_questions = other._questions;
	this->_players = other._players;
}

Game::Game(Game* other)
{
	this->_database = other->_database;
	this->_questions = other->_questions;
	this->_players = other->_players;
}

Game::~Game()
{
}

Question Game::getQuestionForUser(LoggedUser user)
{
	if (this->_players[user].idQuestion == this->_questions.size())
	{
		return Question(); // End. Some Error.
	}

	this->_players[user].currentQuestion = this->_questions[this->_players[user].idQuestion];
	this->_players[user].idQuestion++;
	return this->_players[user].currentQuestion;
}

unsigned int Game::submitAnswer(LoggedUser user, unsigned int idAns, float time)
{
	unsigned int correct = this->_players[user].currentQuestion.idCorrect();
	if (correct == idAns)
	{ // Correct.
		this->_players[user].correctAnswerCount++;
	}
	else
	{ // Wrong.
		this->_players[user].wrongAnswerCount++;
	}

	this->_players[user].averageAnswerTime += time;
	if (this->_players[user].averageAnswerTime != time)
	{ // Not first answer submited.
		this->_players[user].averageAnswerTime /= 2;
	}
	return correct;
}

void Game::removePlayer(LoggedUser user)
{ 
	this->_database->updatePlayer(user, this->_players[user]);
}

std::map<LoggedUser, GameData> Game::getResults()
{
	return this->_players;
}