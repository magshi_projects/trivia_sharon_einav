#pragma once
#include "RequestStructs.h"
#include "json.hpp"
#include <iostream>

#define BUFFER_SIZE 4
#define MSG_TYPE 1

using json = nlohmann::json;

typedef std::vector<std::uint8_t> Buffer;


class deserializeRequest
{
public:
	static json deserialize(Buffer);
	static LoginRequest deserializeLoginRequest(Buffer);
	static SignupRequest deserializeSignupRequest(Buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(Buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(Buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(Buffer);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(Buffer);
};