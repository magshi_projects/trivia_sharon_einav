#include "RoomManager.h"

RoomManager::RoomManager()
{ // doing nothing, map automaticly constructs.
	for(int i = STARTING_ID; i < MAX_ROOMS; i++)
	{ // init all the room ids.
		this->_availableIDs[i] = false;
	}
}

RoomManager::~RoomManager()
{ // clearing all rooms.
	this->_rooms.clear();
}

boost::shared_ptr<Room> RoomManager::createRoom(LoggedUser creator, std::string name, std::string maxPlayers, unsigned int timePerQuestion, unsigned int questionCount)
{ // creating room, and inserting it with it's id.
	int id = NO_SPACE;
	for (auto& it : this->_availableIDs)
	{
		if (!it.second)
		{
			id = it.first;
			it.second = true; // has a room.
			break;
		}
	}

	if (id != NO_SPACE)
	{ // there's space to create room.
		/*Room* room = new Room(id, name, maxPlayers, timePerQuestion, false, questionCount);
		room->addUser(creator); // adding creator.*/
		this->_rooms.insert(std::make_pair(id, boost::make_shared<Room>(
			new Room(id, name, maxPlayers, timePerQuestion, false, questionCount))));
		this->_rooms[id]->addUser(creator); // adding admin.
		return this->_rooms[id];
	}
	return nullptr; // not created.
}

void RoomManager::deleteRoom(unsigned int roomId) 
{ // erasing by key.
	this->_rooms.erase(roomId);
	this->_availableIDs[roomId] = false; // empty space.
}

unsigned int RoomManager::getRoomState(int ID)
{ // returing the isActive variable of room data.
	return this->_rooms[ID]->getRoomData()._isActive;
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> roomsData;
	for (auto& it : this->_rooms)
	{ // autoing through the whole map and getting the rooms' data.
		roomsData.push_back(it.second->getRoomData());
	}
	return roomsData;
}

boost::shared_ptr<Room> RoomManager::getRoom(unsigned int id)
{
	if (this->_rooms.find(id) != this->_rooms.end())
	{
		return this->_rooms[id];
	}
	return nullptr;
}