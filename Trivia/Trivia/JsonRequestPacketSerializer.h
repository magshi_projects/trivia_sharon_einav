#pragma once
#include "JsonRequestPacketDeserializer.h"
#include "json.hpp"
#include "ResponseStructs.h"
#include <iostream>

using json = nlohmann::json;
//typedef std::vector<std::uint8_t> Buffer;

class JsonRequestPacketSerializer
{
public:
	static Buffer serialize(json msg, uint8_t msgType);
	static Buffer serializeResponse(ErrorResponse error);
	static Buffer serializeResponse(LoginResponse result);
	static Buffer serializeResponse(SignupResponse result);
	static Buffer serializeResponse(LogoutResponse response);
	static Buffer serializeResponse(GetRoomsResponse response);
	static Buffer serializeResponse(GetPlayersInRoomResponse response);
	static Buffer serializeResponse(JoinRoomResponse response);
	static Buffer serializeResponse(CreateRoomResponse response);
	static Buffer serializeResponse(HighscoreResponse response);
	static Buffer serializeResponse(CloseRoomResponse response);
	static Buffer serializeResponse(StartGameResponse response);
	static Buffer serializeResponse(GetRoomStateResponse response);
	static Buffer serializeResponse(LeaveRoomResponse response);
	static Buffer serializeResponse(MyStatusResponse response);
	static Buffer serializeResponse(GetQuestionResponse);
	static Buffer serializeResponse(SubmitAnswerResponse);
	static Buffer serializeResponse(GetGameResultsResponse);
	static Buffer serializeResponse(LeaveGameResponse);
};