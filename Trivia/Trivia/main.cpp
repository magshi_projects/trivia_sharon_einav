#include "Server.h"

#define LISTENING_PORT 8888

int main()
{
	try
	{
		Server server(LISTENING_PORT);
		server.run();
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	
	return 0;
}