#pragma once
#include "sqlite3.h"
#include "IDatabase.h"
#include "SQLException.h"
#include <iostream>
#include <map>
#include <string>
#include <list>
#include <fstream>

#define ERR_OPEN_DB "Failed to open DB"
#define DB_NAME "./Trivia.sqlite"

enum Values
{
	ID,
	USERNAME,
	GAMES,
	RIGHT,
	WRONG,
	AVG
};

enum Questions
{
	ID_QUES,
	QUES,
	CORRECT,
	ANS2,
	ANS3,
	ANS4
};

using std::string;

class SqlDatabase : public IDatabase
{
public:

	//General
	SqlDatabase();
	~SqlDatabase();

	//User table
	void createUserTable();
	void addUser(std::string username, std::string password, std::string email);
	void deleteUser(std::string username);
	bool doesUserExist(std::string username);
	bool checkInfo(std::string username, std::string password);
	void clearUserTable();
	std::map<LoggedUser, int> getHighscores();
	std::vector<Question> getQuestions(unsigned int amount);
	void updatePlayer(LoggedUser user, GameData data);
	MyStatus getStatus(LoggedUser user);

	// Callbacks
	friend int callback(void* data, int argc, char** argv, char** azColName);
	friend int highscoreCallback(void* data, int argc, char** argv, char** azColName);
	friend int questionCallback(void* data, int argc, char** argv, char** azColName);
	friend int statusCallback(void* data, int argc, char** argv, char** azColName);

private:
	sqlite3 * _Database;
	std::map<LoggedUser, int> _highscore;
	std::vector<Question> _questions;
	bool _found;
	MyStatus _status;

	std::string addQuotationMarks(std::string val);
	void sqlCommandExecute(std::string& command, int(*callback)(void*, int, char**, char**) = nullptr, void* data = nullptr);
};
