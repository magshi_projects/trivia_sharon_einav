#include "MenuRequestHandler.h"


MenuRequestHandler::MenuRequestHandler(LoggedUser user, RoomManager* roomManager, HighscoreTable* highscoreTable, 
	RequestHandlerFactory* handlerFactory, LoginManager* loginManager, IDatabase* database)
	: _user(user)
{
	this->_roomManager = roomManager;
	this->_highscoreTable = highscoreTable;
	this->_handlerFactory = handlerFactory;
	this->_loginManager = loginManager;
	this->_database = database;
}
                            
MenuRequestHandler::~MenuRequestHandler()
{}


bool MenuRequestHandler::isRequestRelevant(Request& request)
{
	return request.id == ID_LOGOUT || request.id == ID_GET_ROOMS || 
		request.id == ID_PLAYERS_IN_ROOM || request.id == ID_HIGHSCORES ||
		request.id == ID_JOIN_ROOM || request.id == ID_CREATE_ROOM || request.id == ID_MY_STATUS;
}

RequestResult MenuRequestHandler::handleRequest(Request& request)
{
	switch (request.id)
	{
	case ID_LOGOUT:
		return this->signout(request);
		break;
	case ID_GET_ROOMS:
		return this->getRooms(request);
		break;
	case ID_PLAYERS_IN_ROOM:
		return this->getPlayersInRoom(request);
		break;
	case ID_HIGHSCORES:
		return this->getHighscores(request);
		break;
	case ID_JOIN_ROOM:
		return this->joinRoom(request);
		break;
	case ID_CREATE_ROOM:
		return this->createRoom(request);
		break;
	case ID_MY_STATUS:
		return this->getStatus(request);
		break;
	default:
		return RequestResult(); // values == nullptr
		break;
	}
}

RequestResult MenuRequestHandler::getStatus(Request request)
{
	RequestResult result;

	MyStatus status = this->_database->getStatus(this->_user);

	result.response = JsonRequestPacketSerializer::serializeResponse(
		MyStatusResponse(status._games, status._right, status._wrong, status._avg));
	result.newHandler = nullptr; // handler doesn't change, still in menu.
	result.log = this->_user.getUsername() + " - has made a status request.";

	return result;
}

RequestResult MenuRequestHandler::getRooms(Request request)
{
	RequestResult result;

	result.response = JsonRequestPacketSerializer::serializeResponse(
		GetRoomsResponse(true, this->_roomManager->getRooms()));
	result.newHandler = nullptr; // handler doesn't change, still in menu.
	result.log = this->_user.getUsername() + " - requested to get all rooms info.";
	
	return result;
}

RequestResult MenuRequestHandler::getPlayersInRoom(Request request)
{
	GetPlayersInRoomRequest info(deserializeRequest::deserializeGetPlayersRequest(request.buffer));
	RequestResult result;

	result.response = JsonRequestPacketSerializer::serializeResponse(
		GetPlayersInRoomResponse(this->_roomManager->getRoom(info.roomId)->getPlayers()));
	result.newHandler = nullptr; // handler doesn't change, still in menu.
	result.log = this->_user.getUsername() + " - requested to get player in room id: " + std::to_string(info.roomId) + ".";

	return result;
}

RequestResult MenuRequestHandler::getHighscores(Request request)
{
	RequestResult result;

	std::map<LoggedUser, int> highscore_map = this->_highscoreTable->getHighscores();
	result.response = JsonRequestPacketSerializer::serializeResponse(
		HighscoreResponse(highscore_map.size(), highscore_map));
	result.newHandler = nullptr; // handler doesn't change, still in menu.
	result.log = this->_user.getUsername() + " - requested to get highscores table.";

	return result;
}

RequestResult MenuRequestHandler::joinRoom(Request request)
{
	JoinRoomRequest info(deserializeRequest::deserializeJoinRoomRequest(request.buffer));
	RequestResult result;

	boost::shared_ptr<Room> room = this->_roomManager->getRoom(info.roomId);
	bool joinable = !room->isFull();

	if (joinable)
	{ // adding user to room.
		room->addUser(this->_user);
	}
	result.response = JsonRequestPacketSerializer::serializeResponse(
		JoinRoomResponse(joinable));
	result.newHandler = joinable ?
		this->_handlerFactory->createRoomMemberRequestHandler(room, this->_user) // changes to the room (Member).
		: nullptr; // room full.

	result.log = joinable ? this->_user.getUsername() + " - has succesfuly join room: " + std::to_string(info.roomId) + "." :
		this->_user.getUsername() + " - has failed to join room: " + std::to_string(info.roomId) + ".";

	return result;
}

RequestResult MenuRequestHandler::createRoom(Request request)
{
	CreateRoomRequest info(deserializeRequest::deserializeCreateRoomRequest(request.buffer));
	RequestResult result;

	boost::shared_ptr<Room> save = this->_roomManager->createRoom(
		this->_user, info.roomName, info.maxUsers, info.answerTimeout, info.questionCount);

	result.response = JsonRequestPacketSerializer::serializeResponse(CreateRoomResponse(save != nullptr));
	result.newHandler = save != nullptr ?
		this->_handlerFactory->createRoomAdminRequestHandler(save, this->_user) // changes to the room (Admin).
		: nullptr; // no space.
	result.log = save != nullptr ? this->_user.getUsername() + " - has created new room: " + info.roomName + "." :
		this->_user.getUsername() + " - has tried to create room, but failed.";

	return result;
}

RequestResult MenuRequestHandler::signout(Request request)
{
	RequestResult result; 
	bool deleted = this->_loginManager->signout(this->_user);

	result.response = JsonRequestPacketSerializer::serializeResponse(LogoutResponse(deleted));
	result.newHandler = deleted ?
		this->_handlerFactory->createLoginRequestHandler() // yes.
		: nullptr; // no.
	result.log = deleted ? this->_user.getUsername() + " - has succesfuly signed out." :
		this->_user.getUsername() + " - has failed to sign out.";

	return result;
}