#pragma once
#include "IDatabase.h"
#include "SqlDatabase.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"

class Server
{
public:
	Server(int port);
	~Server();

	void run();

private:
	boost::asio::io_context _io_context;
	IDatabase* _database;
	Communicator _communicator;
	RequestHandlerFactory _handlerFactory;
};