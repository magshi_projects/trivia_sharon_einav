#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(
	boost::shared_ptr<Room> room, LoggedUser user, RoomManager* roomManager, GameManager* gameManager, RequestHandlerFactory* handlerFactory)
	: _room(room), _user(user), _roomManager(roomManager), _gameManager(gameManager), _handlerFactory(handlerFactory)
{
}

RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}

bool RoomMemberRequestHandler::isRequestRelevant(Request& request)
{
	return request.id == ID_LEAVE_ROOM || request.id == ID_GET_ROOM_STATE;
}

RequestResult RoomMemberRequestHandler::handleRequest(Request& request)
{
	switch (request.id)
	{
	case ID_LEAVE_ROOM:
		return this->leaveRoom(request);
		break;
	case ID_GET_ROOM_STATE:
		return this->getRoomState(request);
		break;
	default:
		return RequestResult(); // values == nullptr
		break;
	}
}

RequestResult RoomMemberRequestHandler::leaveRoom(Request request)
{
	RequestResult result;

	if (this->_room.get() != nullptr)
	{
		this->_room->removeUser(this->_user);
	}
	result.response = JsonRequestPacketSerializer::serializeResponse(
		CloseRoomResponse(true)); // true because always goes to menu next.
	result.newHandler = this->_handlerFactory->createMenuRequestHandler(this->_user); // returns to menu after closing the room.

	return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(Request request)
{
	RequestResult result;

	if (this->_roomManager->getRoom(this->_room->getId()) != nullptr)
	{ // Room still exists.
		RoomData data = this->_room->getRoomData();

		if (data._isActive)
		{ // Game started, is member.
			result.newHandler = this->_handlerFactory->createGameRequestHandler(
				this->_gameManager->getGame(this->_room.get()), this->_room, this->_user, false);
			result.log = this->_user.getUsername() + " - has been moved to game state, admin started game.";
		}
		else
		{
			result.newHandler = nullptr; // handler doesn't change, still in room.
			//result.log = this->_user.getUsername() + " - has request state of room: " + this->_room->getRoomData()._name + ".";
		}
		result.response = JsonRequestPacketSerializer::serializeResponse(
			GetRoomStateResponse(true, data, this->_room->getPlayers()));
	}
	else
	{ // Room closed.
		result.response = JsonRequestPacketSerializer::serializeResponse(
			GetRoomStateResponse(false));
		result.newHandler = this->_handlerFactory->createMenuRequestHandler(this->_user); // goes to menu because room is closed.
		result.log = this->_user.getUsername() + " - has been moved to menu, admin closed room.";
	}

	return result;
}
