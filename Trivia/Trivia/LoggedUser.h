#pragma once
#include <string>

class LoggedUser
{
public:
	LoggedUser(std::string username);
	~LoggedUser();
	
	std::string getUsername();
	std::string getUsername() const;
	void setUsername(std::string name);

	bool operator<(const LoggedUser& obj) const;
	bool operator==(const LoggedUser& obj) const;

private:
	std::string _username;
};