#include "SqlDatabase.h"

SqlDatabase::SqlDatabase()
{
	bool openFailed = false; //Open flag

	string dbFileName = DB_NAME; //parameter string for open func, store the db name
	int result = sqlite3_open(dbFileName.c_str(), &this->_Database); //Open the DB fike func
	openFailed = result != SQLITE_OK; //Check if data file opened
	if (openFailed) //If failed to open
	{
		this->_Database = NULL; //Null
		std::cout << ERR_OPEN_DB << std::endl; //Error print
		throw SQLException();
	}

	this->createUserTable();
}

SqlDatabase::~SqlDatabase()
{
	sqlite3_close(this->_Database); //Close connection
	this->_Database = NULL; //Destroy the pointer
}

void SqlDatabase::createUserTable()
{
	string createUserTable = R"(CREATE TABLE IF NOT EXISTS Users(
								ID INTEGER PRIMARY KEY AUTOINCREMENT,
								Username TEXT NOT NULL,
								Password TEXT NOT NULL,
								Email TEXT NOT NULL);)";

	string createQuestionTable = R"(CREATE TABLE IF NOT EXISTS Questions(
							ID INTEGER PRIMARY KEY AUTOINCREMENT,
							Question TEXT NOT NULL,
							Correct TEXT NOT NULL,
							Ans2 TEXT NOT NULL,
							Ans3 TEXT NOT NULL,
							Ans4 TEXT NOT NULL);)";

	string createScoreTable = R"(CREATE TABLE IF NOT EXISTS Scores(
					   ID INTEGER PRIMARY KEY AUTOINCREMENT,
					   User TEXT NOT NULL,
					   Games INTEGER DEFAULT 0,
					   Right INTEGER DEFAULT 0,
					   Wrong INTEGER DEFAULT 0,
					   Avg FLOAT DEFAULT 0.0,
					   FOREIGN KEY (User) REFERENCES Users(Username) ON DELETE CASCADE);)";

	sqlCommandExecute(createUserTable);
	sqlCommandExecute(createQuestionTable);
	sqlCommandExecute(createScoreTable);
}

// Callbacks

int callback(void* data, int argc, char** argv, char** azColName)
{ // called when ever theres a select that passed the conditions.
	SqlDatabase* obj = (SqlDatabase*)data;

	obj->_found = true; // found the wanted value.
	return 0;
}

int highscoreCallback(void* data, int argc, char** argv, char** azColName)
{
	SqlDatabase* obj = (SqlDatabase*)data;

	LoggedUser user(argv[USERNAME]);
	int score = std::stoi(argv[RIGHT]);

	obj->_highscore[user] = score; // adding to map of top users.

	return 0;
}

int questionCallback(void* data, int argc, char** argv, char** azColName)
{
	SqlDatabase* obj = (SqlDatabase*)data;

	Question ques(argv[QUES], argv[CORRECT], argv[ANS2], argv[ANS3], argv[ANS4]);
	obj->_questions.push_back(ques);

	return 0;
}

int statusCallback(void* data, int argc, char** argv, char** azColName)
{
	SqlDatabase* obj = (SqlDatabase*)data;

	obj->_status._games = argv[GAMES];
	obj->_status._right = argv[RIGHT];
	obj->_status._wrong = argv[WRONG];
	obj->_status._avg = argv[AVG];

	return 0;
}

//Adds user into User table
void SqlDatabase::addUser(string username, string password, string email)
{
	string addUser = "INSERT INTO Users\
						  (Username, Password, Email) VALUES(";
	addUser += addQuotationMarks(username) + "," + addQuotationMarks(password) + "," + addQuotationMarks(email) + ");";
	sqlCommandExecute(addUser);

	string addScore = "INSERT INTO Scores\
						(User) VALUES(";
	addScore += addQuotationMarks(username) + ");";
	sqlCommandExecute(addScore);
}

void SqlDatabase::deleteUser(string username)
{
	string deleteUser = "DELETE FROM Users WHERE Username LIKE ";
	deleteUser += addQuotationMarks(username) + ";";
	sqlCommandExecute(deleteUser);
}

bool SqlDatabase::doesUserExist(string username)
{
	this->_found = false;
	string str = "SELECT * FROM Users\
					WHERE username LIKE '" + username + "';";

	sqlCommandExecute(str, callback, this);
	return this->_found;
}

bool SqlDatabase::checkInfo(std::string username, std::string password)
{
	this->_found = false;
	string str = "SELECT * FROM Users\
					WHERE username LIKE '" + username + "'\
					AND password LIKE '" + password + "';";

	sqlCommandExecute(str, callback, this);
	return this->_found;
}

void SqlDatabase::updatePlayer(LoggedUser user, GameData data)
{
	string str = "UPDATE Scores\
					SET Games = Games + 1,\
						Right = Right + " + std::to_string(data.correctAnswerCount) + ",\
						Wrong = Wrong + " + std::to_string(data.wrongAnswerCount) + ",\
						Avg = (Avg + " + std::to_string(data.averageAnswerTime) + ") / 2\
					WHERE user LIKE '" + user.getUsername() + "';";

	sqlCommandExecute(str);
}

MyStatus SqlDatabase::getStatus(LoggedUser user)
{
	string str = "SELECT * FROM Scores\
				WHERE User LIKE '" + user.getUsername() + "';";

	sqlCommandExecute(str, statusCallback, this);
	return this->_status;
}

std::map<LoggedUser, int> SqlDatabase::getHighscores()
{
	this->_highscore.clear();

	string str = "SELECT * FROM Scores\
					ORDER BY right DESC\
					LIMIT 10;";

	sqlCommandExecute(str, highscoreCallback, this);
	return this->_highscore;
}

std::vector<Question> SqlDatabase::getQuestions(unsigned int amount)
{
	this->_questions.clear();

	string str = "SELECT * FROM Questions\
					ORDER BY RANDOM() LIMIT " + std::to_string(amount) + ";";

	sqlCommandExecute(str, questionCallback, this);
	return this->_questions;
}

void SqlDatabase::clearUserTable()
{
	string clear_cmd = "DELETE FROM Users;";
	sqlCommandExecute(clear_cmd);
}

/*
Param: value (string)
Adds " " between value
Returns "val"
*/
string SqlDatabase::addQuotationMarks(string val)
{
	char quotationMark = '"';
	return (quotationMark + val + quotationMark);
}

//Executes command in param and check for sql error msg
void SqlDatabase::sqlCommandExecute(string& command, int(*callback)(void*, int, char**, char**), void* data)
{
	char** errMsg = nullptr;
	int res = sqlite3_exec(this->_Database, command.c_str(), callback, data, errMsg); //exec command
	if (res != SQLITE_OK) //Error
	{
		throw SQLException();
	}
}