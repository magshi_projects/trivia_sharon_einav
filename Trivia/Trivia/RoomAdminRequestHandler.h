#pragma once
#include "IRequestHandler.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "HighscoreTable.h"

#include <boost/shared_ptr.hpp>

class RequestHandlerFactory;
class RoomManager;
class GameManager;

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(boost::shared_ptr<Room> room, LoggedUser user, RoomManager* roomManager, GameManager* gameManager, RequestHandlerFactory* handlerFactory);
	~RoomAdminRequestHandler();

	virtual bool isRequestRelevant(Request& request);
	virtual RequestResult handleRequest(Request& request);

private:
	boost::shared_ptr<Room> _room;
	LoggedUser _user;
	GameManager* _gameManager;
	RoomManager* _roomManager;
	RequestHandlerFactory* _handlerFactory;

	RequestResult closeRoom(Request request);
	RequestResult startGame(Request request);
	RequestResult getRoomState(Request request);
};