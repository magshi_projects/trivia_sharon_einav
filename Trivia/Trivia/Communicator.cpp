#include "Communicator.h"


Communicator::Communicator(RequestHandlerFactory* handlerFactory, boost::asio::io_context& io_context, int port)
	: _acceptor(io_context, tcp::endpoint(tcp::v4(), port)), _io_context(io_context)
{
	this->_handlerFactory = handlerFactory;
	this->_running = true;
	this->_port = port;
}

Communicator::~Communicator()
{
	this->_running = false;
	for (auto& it : this->_clients)
	{
		if (it.second != nullptr)
		{
			delete it.second;
		}
	}
}

void Communicator::bindAndListen()
{
	// making thread for client management.
	boost::thread manager_thread(&Communicator::client_manager, this);
	manager_thread.detach(); // letting it be free.

	std::cout << "listening... in port - " << _port << std::endl; // console log.
	while (_running)
	{
		boost::shared_ptr<tcp::socket> socket(new tcp::socket(this->_io_context));
		this->_acceptor.accept(*socket); // accepting clients.
		std::cout << "accepted new client. IP address - "
			<< socket->remote_endpoint().address() << std::endl;

		this->appendClients(socket); // appending to map with handler, and to pointer vector.
		this->_manager_sleep.notify_all(); // waking up the client manager.
	}
}

void Communicator::client_manager()
{
	// created thread pool.
	boost::asio::thread_pool pool(NUM_THREADS);
	std::mutex mutex;
	std::unique_lock<std::mutex> lock(mutex);

	while (_running)
	{
		if (this->_clients.empty())
		{ // sleeping in there are not clients.
			this->_manager_sleep.wait(lock);
		}

		this->_client_lock.lock();
		for (auto& it : this->_clients)
		{
			// checking if there's data.
			size_t read_size = it.first->available();
			if (read_size > NULL)
			{
				this->post(it.first, pool); // may need to change
			}
		}
		this->_client_lock.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(TIME_SLEEP));
	}
}

void Communicator::post(boost::shared_ptr<tcp::socket> socket, boost::asio::thread_pool& pool)
{
	// submit a lambda object to the pool.
	boost::asio::post(pool,
		[socket, this]
		{
			IRequestHandler* handler = this->_clients[socket];
			Request request;

			request = this->recvRequest(socket);

			if (request.id == CLIENT_EXIT) // client exited cleanly.
			{ // locking, erasing client, and unlocking.
				this->_client_lock.lock();
				this->_clients.erase(socket);
				this->_client_lock.unlock();
			}
			else
			{ // when there is a client, let's see the request he has.
				if (!handler->isRequestRelevant(request))
				{ // send error message.
					this->sendError(socket, ERROR_IRELEVENT);
				}
				else
				{
					RequestResult result = handler->handleRequest(request);
					if (result.newHandler != nullptr)
					{ // if it's not nullptr, then succes and moving to next handler.
						delete this->_clients[socket]; // clearing.
						this->_clients[socket] = result.newHandler;
					}

					// here we send the buffer.
					if (!result.log.empty())
					{ // printing log.
						std::cout << result.log << std::endl;
					}
					this->sendResponse(socket, result);
				}
			}
		});
}

void Communicator::appendClients(boost::shared_ptr<tcp::socket>& socket)
{ // adding to map with new IRequestHandler.
	this->_client_lock.lock();
	this->_clients[socket] = this->_handlerFactory->createLoginRequestHandler();
	this->_client_lock.unlock();
}

void Communicator::sendResponse(boost::shared_ptr<tcp::socket> socket, RequestResult result)
{ // sending data in result response.
	boost::system::error_code ignored_error;
	boost::asio::write(*socket, boost::asio::buffer(result.response.data(), result.response.size()),
		boost::asio::transfer_all(), ignored_error);
}

void Communicator::sendError(boost::shared_ptr<tcp::socket> socket, std::string error)
{
	RequestResult result;
	result.newHandler = nullptr;
	result.response = JsonRequestPacketSerializer::serializeResponse(ErrorResponse(error));
	Communicator::sendResponse(socket, result);
}

Request Communicator::recvRequest(boost::shared_ptr<tcp::socket> socket)
{
	Request save;
	boost::array<uint8_t, 1024> buf;
	boost::system::error_code error;

	size_t len = socket->read_some(boost::asio::buffer(buf), error);

	if (error == boost::asio::error::eof)
	{ // Connection closed cleanly by peer.
		save.id = CLIENT_EXIT;
		return save; // Some other error.
	}

	Buffer v(std::begin(buf), std::begin(buf) + len); // collect only the data that was sent.
	return Communicator::extractRequest(v); // reference, then needs to be a local pass and not ivale.
}

Request Communicator::extractRequest(Buffer& buffer)
{
	Request request;

	request.id = buffer[ID_PLACEMENT];
	request.receivalTime = Communicator::make_daytime();
	request.updateBuffer(buffer);

	return request;
}

std::string Communicator::make_daytime()
{
	time_t now = time(0);
	return ctime(&now);
}