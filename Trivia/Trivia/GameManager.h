#pragma once
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "IDatabase.h"
#include "Game.h"
#include "Room.h"

class GameManager
{
public:
	GameManager(IDatabase* database);
	~GameManager();

	boost::shared_ptr<Game> createGame(Room room);
	void deleteGame(Room room);

	boost::shared_ptr<Game> getGame(Room room);

private:
	IDatabase* _database;
	std::map<Room, boost::shared_ptr<Game>> _games;
};